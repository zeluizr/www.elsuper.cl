/*! Cookie - v1.0.0 - 2016-05-06 * Copyright(c) 2016 Ze Luiz Rodrigues */
function setCookie(a, b, c) { var d = new Date; d.setTime(d.getTime() + 24 * c * 60 * 60 * 1e3); var e = "expires=" + d.toGMTString(); document.cookie = a + "=" + b + "; " + e } function getCookie(a) { for (var b = a + "=", c = document.cookie.split(";"), d = 0; d < c.length; d++) { for (var e = c[d]; " " == e.charAt(0);)e = e.substring(1); if (0 == e.indexOf(b)) return e.substring(b.length, e.length) } return "" }
/*! Guardar carrito con API Hubin - Wevo - v1.0.0 - 2018-01-23 * Copyright(c) 2018 Ze Luiz Rodrigues */
function saveList(orderForm, nombre) {
    var TokenUsuario = getCookie("VtexIdclientAutCookie_elsuper");
    var items = orderForm.items;
    var json = { "Nome": nombre, "Produtos": [], "TokenUsuario": TokenUsuario }

    $.each(items, function (i, data) {
        json.Produtos.push({ "IdVtexSku": data.id, "Quantidade": data.quantity });
    });

    $(".error-modale").text("Guardando tu lista!");

    $.ajax({
        headers: {
            "x-hubin-chef-api-appkey": "elsuperapi",
            "x-hubin-chef-api-apptoken": "HTJK571CD8B03AAD201A6BBA455B93CAAD0ODHT",
            "Content-Type": "application/json"
        },
        data: JSON.stringify(json),
        type: "POST",
        url: "https://elsuper.hubin.io/chef/api/listacompra/listausuariosalvar",
        success: function (data) {
            $(".save-cart").text("Tu lista fue creada.");
            setTimeout(function () {
                $(".save-cart").text("¿Quieres hacer de este carro una lista?");
            }, 5000);
            $(".blackboard").remove();
            $(".whiteboard .boxing-head #guardar").removeAttr("disabled");
        },
        error: function (error) {
            $(".error-modale").text("Hola, ya tiene una lista de compras con ese nombre.");
            $(".whiteboard .boxing-head #guardar").removeAttr("disabled");
        }
    });
}

function createModal(orderForm) {
    var html = '<div class="blackboard"><div class="whiteboard"><div class="header"><h2 class="title">Guardar mi lista</h2><span class="close"><img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTcuMS4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDMxMC4yODUgMzEwLjI4NSIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMzEwLjI4NSAzMTAuMjg1OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjE2cHgiIGhlaWdodD0iMTZweCI+CjxwYXRoIGQ9Ik0yNjQuODQ1LDQ1LjQ0MUMyMzUuNTQyLDE2LjEzOSwxOTYuNTgyLDAsMTU1LjE0MywwUzc0Ljc0MywxNi4xMzksNDUuNDQsNDUuNDQxQzE2LjEzOCw3NC43NDMsMCwxMTMuNzAzLDAsMTU1LjE0MyAgYzAsNDEuNDM5LDE2LjEzOCw4MC4zOTksNDUuNDQsMTA5LjcwMWMyOS4zMDMsMjkuMzAzLDY4LjI2Miw0NS40NCwxMDkuNzAyLDQ1LjQ0czgwLjM5OS0xNi4xMzgsMTA5LjcwMi00NS40NCAgYzI5LjMwMi0yOS4zMDIsNDUuNDQtNjguMjYyLDQ1LjQ0LTEwOS43MDFDMzEwLjI4NSwxMTMuNzAzLDI5NC4xNDcsNzQuNzQzLDI2NC44NDUsNDUuNDQxeiBNMTg5LjcwMiwxNTcuOTg1bDM4LjY2OSwzOC42NjkgIGMzLjEyLDMuMTE5LDMuMTIsOC4xOTQsMCwxMS4zMTNsLTIyLjMzMywyMi4zMzNjLTEuNTA3LDEuNTA3LTMuNTE2LDIuMzM3LTUuNjU3LDIuMzM3Yy0yLjE0MSwwLTQuMTUtMC44My01LjY1Ny0yLjMzNyAgbC0zOC42NjktMzguNjY5Yy0wLjc0OC0wLjc0Ni0yLjA4LTAuNzQ2LTIuODI5LDBsLTM4LjY2OSwzOC42NjljLTEuNTA3LDEuNTA3LTMuNTE2LDIuMzM3LTUuNjU3LDIuMzM3ICBjLTIuMTQxLDAtNC4xNDktMC44My01LjY1Ny0yLjMzNmwtMjIuMzMzLTIyLjMzNGMtMS41MDctMS41MDctMi4zMzctMy41MTYtMi4zMzctNS42NTZjMC0yLjE0MiwwLjgzLTQuMTUsMi4zMzctNS42NTcgIGwzOC42NjktMzguNjY5YzAuNzY2LTAuNzY3LDAuNzY2LTIuMDYzLTAuMDAxLTIuODI5bC00MC4zMDItNDAuMzAyYy0xLjUwNy0xLjUwNy0yLjMzNy0zLjUxNi0yLjMzNy01LjY1NyAgYzAtMi4xNDEsMC44My00LjE0OSwyLjMzNy01LjY1NmwyMi4zMzMtMjIuMzMzYzEuNTA3LTEuNTA3LDMuNTE2LTIuMzM3LDUuNjU3LTIuMzM3YzIuMTQxLDAsNC4xNDksMC44Myw1LjY1NiwyLjMzN2w0MC4zMDMsNDAuMzAzICBjMC43NDksMC43NDcsMi4wODEsMC43NDYsMi44MjgsMGw0MC4zMDMtNDAuMzAzYzEuNTA3LTEuNTA3LDMuNTE2LTIuMzM3LDUuNjU3LTIuMzM3YzIuMTQxLDAsNC4xNDksMC44Myw1LjY1NiwyLjMzN2wyMi4zMzMsMjIuMzMzICBjMS41MDcsMS41MDcsMi4zMzcsMy41MTYsMi4zMzcsNS42NTZjMCwyLjE0Mi0wLjgzLDQuMTUtMi4zMzcsNS42NThsLTQwLjMwMiw0MC4zMDEgIEMxODguOTM2LDE1NS45MjMsMTg4LjkzNiwxNTcuMjE5LDE4OS43MDIsMTU3Ljk4NXoiIGZpbGw9IiMwMDAwMDAiLz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPC9zdmc+Cg==" /></span></div><div class="pregunta"><p>Esta lista será guardada para que la puedas usar a futuro como una de tus lists base.<br />Ponle un nombre para que la indentifiques fácilmente.</p></div><div class="boxing-head"><input type="text" name="nombre-lista" id="nombre-lista" /><button id="guardar">Guardar</button></div><span class="error-modale"></span></div></div>';
    $(html).prependTo("body");
    $(".whiteboard .close").click(function () {
        $(".blackboard").remove();
    });
    $(".whiteboard .boxing-head #guardar").click(function () {
        $(".whiteboard .boxing-head #guardar").attr('disabled', 'disabled');
        var nombre = $(".whiteboard .boxing-head #nombre-lista").val();
        if (nombre == "")
            $(".error-modale").text("Por favor elija un nombre a tu lista.");
        else
            saveList(orderForm, nombre);

    });
}

$(function () {
    $('<link rel="stylesheet" type="text/css" href="/arquivos/elsuper.css">').prependTo("body");
    vtexjs.checkout.getOrderForm().done(function (orderForm) {
        if (orderForm.clientProfileData !== null) {
            var email = orderForm.clientProfileData.email;
            $(".save-cart").css("visibility", "visible");
            $(".save-cart").on("click", function () {
                createModal(orderForm);
            });
        }
    });
});