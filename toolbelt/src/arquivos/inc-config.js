var __json;
var __$json;
/*! Cookie - v1.0.0 - 2016-05-06 * Copyright(c) 2016 Ze Luiz Rodrigues */
function setCookie(a, b, c) { var d = new Date; d.setTime(d.getTime() + 24 * c * 60 * 60 * 1e3); var e = "expires=" + d.toGMTString(); document.cookie = a + "=" + b + "; " + e } function getCookie(a) { for (var b = a + "=", c = document.cookie.split(";"), d = 0; d < c.length; d++) { for (var e = c[d]; " " == e.charAt(0);)e = e.substring(1); if (0 == e.indexOf(b)) return e.substring(b.length, e.length) } return "" }

function noTitle() {
	document.title = "elSuper Cyber | ¡Productos con hasta 60% de descuento!";
}

function modalDelivery() {
	if (getCookie("delivery") != "true") {
		$(".modal-compra").css("display", "flex");
		setCookie("delivery", true, 7);
	}
	$(".modal-compra .close").click(function () {
		$(".modal-compra").css("display", "none");
	});
}

function menuDestacado(link, elem, catg) {
	$.ajax({
		url: "/api/catalog_system/pub/facets/search/categorias/?map=c&fq=H:" + link,
	}).done(function (result) {
		try {
			var categories = result.CategoriesTrees;
			// console.log(result);
			for (var i = 0; i <= 5; i++) {
				var _html = '<li class="sub-item {{#lowercase Name}}{{this}}{{/lowercase}}"><a href="{{Link}}/' + link + '?O=OrderByBestDiscountDESC&map=c,c,c,productClusterIds" class="sub-link">en {{Name}}</a></li>';
				handlebarsjs(_html, categories[i], elem, "append");
			}
			var _html = '<li class="sub-item ver-mas"><a href="/busca?fq=H:' + link + '" class="sub-link">Ver todos los "' + catg + '"</a></li>';
			handlebarsjs(_html, categories[i], elem, "append");
		} catch (error) {

		}
	}).error(function (e) {
		// console.log(e);
		return false;
	});
}

// recupero de carro
function clickRecupero() {
	$("#recupero").click(function () {
		recupero($("#valor").val());
	});
}
function recupero(email) {
	$.ajax({
		url: "//api.vtex.com/elsuper/dataentities/CG/search?_fields=items&_where=email=" + email,
	}).done(function (result) {
		try {
			var url = "http://www.elsuper.cl/checkout/cart/add?";
			// var json = $.parseJSON(result[0].items);
			var json = result[0].items;
			var _url = url;
			for (var i = 0; i < json.length; i++) {
				if (i == (json.length - 1)) {
					_url = _url + "sku=" + json[i].id + "&qty=" + json[i].quantity + "&seller=1"
				} else {
					_url = _url + "sku=" + json[i].id + "&qty=" + json[i].quantity + "&seller=1&"
				}
			}
			$(".content").html("<a href='" + _url + "' target='_blank'>Recuperar CARRO!</a>")

		} catch (error) {
			$(".content").html("<h1>No puedo recuperar el carro, por favor llame a Zé ;)</h1>");
		}
	}).error(function (e) {
		// console.log(e);
		return false;
	});
}
// no headers numbers
function noheadernumbers() {
	$(".search-single-navigator h4 a").each(function (i, e) {
		$(this).text($(this).attr("title"));
	});
}
// functions globais
function global() {
	// if ($(".title-tab").length > 0) {
	// 	document.title = $(".title-tab").text() + " - elSuper.cl";
	// }
	$(".helperComplement").remove();
	if ($("body").hasClass("home")) {
		$(".hamburger").addClass("active");
	}
	if ($("body").hasClass("category")) {
		$(".bread-crumb .last").remove();
	}
}
// handlebarsjs
function handlebarsjs(template, content, elemento, _html) {
	Handlebars.registerHelper('reverse', function (arr) {
		arr.reverse();
	});
	Handlebars.registerHelper("counter", function (index) {
		return index;
	});
	Handlebars.registerHelper('lowercase', function (a) {
		return tolowercase(a);
	});
	Handlebars.registerHelper('pricing', function (a) {
		return accounting.formatMoney(adjustMoneda(a), "$ ", 0, ".", ",");
	});
	var template = Handlebars.compile(template);
	var html = template(content);
	if (_html == "html") {
		$(elemento).html(html);
	} else if (_html == "append") {
		$(html).appendTo(elemento);
	}
}
// lowercase function
function tolowercase(s) {
	var r = s.toLowerCase();
	r = r.replace(new RegExp("[àáâãäå]", 'g'), "a");
	r = r.replace(new RegExp("æ", 'g'), "ae");
	r = r.replace(new RegExp("ç", 'g'), "c");
	r = r.replace(new RegExp("[èéêë]", 'g'), "e");
	r = r.replace(new RegExp("[ìíîï]", 'g'), "i");
	r = r.replace(new RegExp("ñ", 'g'), "n");
	r = r.replace(new RegExp("[òóôõö]", 'g'), "o");
	r = r.replace(new RegExp("œ", 'g'), "oe");
	r = r.replace(new RegExp("[ùúûü]", 'g'), "u");
	r = r.replace(new RegExp("[ýÿ]", 'g'), "y");
	r = r.replace(new RegExp(",", 'g'), "");
	return r.split(' ').join('-');
}
// select comuna
function communa() {
	comuna();
	$(".commune-select .label").click(function () {
		$(this).next(".options").slideToggle("slow");
	});
	$(document).on("click", ".commune-select .options .option", function () {
		$(".comunas-popup").css("visibility", "hidden");
		var _this = $(this);
		var _neighborhood = _this.text();
		var _postalCode = _this.data("postalcode");
		var _state = _this.data("state");
		var _name = _this.data("name");
		vtexjs.checkout.getOrderForm().then(function (orderForm) {
			var shippingData = orderForm.shippingData;
			if (!shippingData) shippingData = {};
			if (!shippingData.address) shippingData.address = {};
			$.extend(shippingData.address, {
				state: _state,
				country: "CHL",
				neighborhood: _neighborhood,
				postalCode: _postalCode,
				postalCodeIsValid: true
			});
			return vtexjs.checkout.sendAttachment('shippingData', shippingData);
		}).done(function () {
			setCookie("commune", _name, 999);
			setCookie("communeZIP", _postalCode, 999);
		});
		$(".step-1 .select").show();
		$(".step-1 .select .text .place").text(_neighborhood);
		_this.parent().hide("fast");
		refreshCart();
		minicart();
	});
	$(".change-commune").unbind().click(function () {
		$(".step-1 .select").hide();
		setCookie("commune", "", 0);
		setCookie("communeZIP", "", 999);
		refreshCart();
		minicart();
	});
	setTimeout(function () {
		var $commune = getCookie("commune");
		if ($commune.length > 0) {
			$(".option[data-name=" + $commune + "]").trigger("click");
			$("#comunas .loading").fadeOut("fast");
		} else {
			$(".option[data-name='las-condes']").trigger("click");
			$("#comunas .loading").fadeOut("fast");
		}
	}, 1000);
}
// buscador
function buscador() {
	$('input[name="buscar"]').focus(function () {
		if ($(this).val().length >= 3) {
			$(".result").show(0);
		}
		$("[rel='/no-cache/callcenter/disclaimer']").fadeIn(0);
		$(".search-result").show(0);
		$(".lista").hide(0);
	});
	$("[rel='/no-cache/callcenter/disclaimer']").click(function () {
		$("[rel='/no-cache/callcenter/disclaimer']").hide(0);
		$(".search-result").hide(0);
		$(".result").hide(0);
		$(".no-result").hide(0);
		$(".lista").hide(0);
	});
	$('input[name="buscar"]').keyup(function (e) {
		e.preventDefault();
		var $this = this;
		setTimeout(buscandoParametros($this, e), 600);
	});
	$("#listado").keypress(function (e) {
		var _this = $(this);
		var keyMap = {
			13: "enter",
			44: "coma",
			32: "space"
		}
		var key = event.which in keyMap ? keyMap[event.which] : "default";
		var value = _this.val();
		switch (key) {
			case "enter":
				value = value.trim();
				if (value[value.length - 1] != "," && value.match(/\S/) != null) value += ",";
				if (value.length) value += " "
				_this.val(value);
				return false;
				break;
		}
	});
	$(".buscar-varios .lista .bottom a").click(function () {
		$("#listado").val("").focus();
	});
	$(".buscar-varios .lista .bottom button").click(function () {
		var _lista = $("#listado").val(),
			_lista = _lista.replace(/\n/g, ",");
		if (_lista.slice(-1) == ",") {
			_lista = _lista.slice(0, -1);
		}
		window.location = "/busqueda?lista="+_lista;
	});
	$(".buscar-varios .lista .top .close").click(function () {
		$("[rel='/no-cache/callcenter/disclaimer']").hide(0);
		$(".lista").hide(0);
	});
	$("#buscarvarios").click(function () {
		$("[rel='/no-cache/callcenter/disclaimer']").show(0);
		$(".lista").show(0);
		$("#listado").focus();
		$(".result").hide(0);
		$(".no-result").hide(0);
	});
}
function buscandoParametros($this, e) {
	if ($this.value.length >= 3) {
		var _this = $this.value;
		if (e.which == 13) window.location.href = "/busca?ft=" + _this + "&O=OrderByBestDiscountDESC";
		$.ajax({
			url: "/buscaautocomplete/?maxRows=10&productNameContains=" + _this,
			success: function (data) {
				$(".search-categorias").empty();
				$(".search-products").empty();
				var itemsReturned = data.itemsReturned;
				if (itemsReturned.length > 0) {
					$(".result").show(0);
					$(".no-result").hide(0);
					itemsReturned.forEach(function (results) {
						if (results.criteria !== null) {
							var text = results.name,
								text = text.split("  "),
								text = "<a class='categoria-search' href='" + results.href + "'><span class='red'>" + text[0] + "</span></a>";
							var _li = $("<li />").html(text).addClass("categorias");
							$(".search-categorias").append(_li);
						} else if (results.criteria === null) {
							var text = results.name,
								text = "<a class='categoria-search' href='" + results.href + "'>" + text + "</span></a>";
							var _li = $("<li />").html(text).addClass("categorias");
							$(".search-categorias").append(_li);
						}
						// $.ajax({
						// 	url: "/api/catalog_system/pub/products/search/" + _this,
						// 	success: function (data) {
						// 		var _data = data[0].items;
						// 		_data.forEach(function (sku) {
						// 			if (sku.sellers[0].commertialOffer.AvailableQuantity > 0) {
						// 				var _price = accounting.formatMoney(sku.sellers[0].commertialOffer.Price, "$ ", 0, ".", ",");
						// 				var _lprice = accounting.formatMoney(sku.sellers[0].commertialOffer.ListPrice, "$ ", 0, ".", ",");
						// 				if (sku.sellers[0].commertialOffer.Price != sku.sellers[0].commertialOffer.ListPrice) {
						// 					var _princing = '<span class="price"><s>' + _lprice + '</s></span><span class="lprice">' + _price + '</span>';
						// 				} else {
						// 					var _princing = '<span class="price">' + _price + '</span>';
						// 				}
						// 				var _img = sku.images[0].imageTag.replace(/#width#/, "150").replace(/#height#/, "150").replace("~", "");
						// 				var _html = '<li class="items"><div class="img"><a href="' + data[0].link + '">' + _img + '</a></div><div class="name-price"><a class="name" href="' + data[0].link + '">' + sku.name + '</a><a class="price" href="' + data[0].link + '">' + _princing + '</a></div><div class="buy"><div class="qty"><input type="text" name="amout" id="amout" value="1" maxlength="2" /><div class="counter"><span class="up"><i class="fa fa-caret-up" aria-hidden="true"></i></span><span class="down"><i class="fa fa-caret-down" aria-hidden="true"></i></span></div></div><button class="buysku" data-id="' + sku.itemId + '"><span class="okay"><i class="fa fa-cart-plus" aria-hidden="true"></i></span><span class="cargando"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i></span></button></div></li>';
						// 				$(".search-products").append(_html);
						// 			}
						// 		});
						// 	}
						// });
					});
				} else if (itemsReturned.length <= 0) {
					$(".result").hide(0);
					$(".no-result").show(0);
				}
			}
		});
		$(".all-results").attr("href", "/busca?ft=" + _this + "&O=OrderByBestDiscountDESC");
	} else {
		$(".result").hide(0);
		$(".no-result").hide(0);
	}
}
// ajax menu
function menu() {
	$.ajax({
		url: "/api/catalog_system/pub/category/tree/1/"
	}).done(createMenu);
}
// ajax sidekick
function sidekick() {
	$.ajax({
		url: "//api.vtex.com/qaelsuper/dataentities/MN/search?_fields=name,link,icon,order&_sort=order"
	}).done(createSidekick);
}
// ajax newsletter
function newsletter() {
	$(".newsletter #send").on("click", function (e) {
		// avoid(0);
		var datos = {};
		datos.isNewsletterOptIn = true;
		datos.email = $("input#email").val();
		$.ajax({
			headers: {
				"Accept": "application/json; charset=utf-8",
				"Content-Type": "application/json; charset=utf-8"
			},
			data: JSON.stringify(datos),
			type: 'PATCH',
			url: '//api.vtexcrm.com.br/elsuper/dataentities/CL/documents',
			success: function (data) {
				// $(".messages").text("Gracias por su inscripción.").removeClass("error");
				$("input#email").val("");
			},
			error: function (error) {
				var json = jQuery.parseJSON(error.responseText);
				var $error = json.ExceptionMessage;
				switch ($error) {
					case "O documento já existe":
						$error = "Ya está subscrito."
						break;
					case "O campo 'Email' deve ser preenchido.":
						$error = "Por favor completa con un email válido."
						break;
					default:
						$error = "El mail ingresado es inválido, por favor verifícalo."
						break;
				}
				$(".messages").text($error).addClass("error");
				console.log($error);
			}
		});
	});
}
// ajax communa
function comuna() {
	$.ajax({
		url: "//api.vtex.com/qaelsuper/dataentities/CM/search?_fields=name,postalCode,state&_where=active=true&_sort=name"
	}).done(function (data) {
		data.forEach(function (element) {
			var _html = '<div data-name="' + tolowercase(element.name) + '" class="option" data-active="' + element.active + '" data-postalcode="' + element.postalCode + '" data-state="' + element.state + '">' + element.name + '</div>';
			$(".commune-select .options").append(_html);
		});
	});
}
function subSubMenu(_department, _ul) {
	$.ajax({
		url: "/api/catalog_system/pub/facets/search/" + tolowercase(_department) + "?map=c"
	}).done(function (data) {
		data.CategoriesTrees.forEach(function (categories) {
			var _name = tolowercase(categories.Name);
			_department = tolowercase(_department);
			if (_department == _name) {
				console.log(categories.Children);
				var _html = '<li class="item {{#lowercase this.Name}}{{this}}{{/lowercase}}"><a href="{{#lowercase this.Link}}{{this}}{{/lowercase}}" class="itemlink">{{this.Name}}</a><ul class="submenu">{{#each this.Children}}<li class="subitem {{#lowercase this.Name}}{{this}}{{/lowercase}}"><a href="{{#lowercase this.Link}}{{this}}{{/lowercase}}" class="sublink">{{Name}}</a>{{#if this.Children}}<ul class="sub-menu">{{#each this.Children}}<li class="sub-item {{#lowercase this.Name}}{{this}}{{/lowercase}}"><a href="{{#lowercase this.Link}}{{this}}{{/lowercase}}" class="sub-link">{{Name}}</a></li>{{/each}}</ul>{{/if}}</li>{{/each}}</ul></li>';
				handlebarsjs(_html, categories, _ul, "append");
			}
		});
	});
}
// create menu
function createMenu(data) {
	var _ul = $("<ul />").addClass("nav");
	_ul.appendTo("#menu");
	for (var i = 0; i < data.length; i++) {
		if (data[i].id !== 303) {
			var _department = data[i].name;
			subSubMenu(_department, _ul);
		}
	}
}
// create categorys on departments
function createDepartmentChildren() {
	var _category = tolowercase(vtxctx.departmentName);
	$.ajax({
		url: "/api/catalog_system/pub/facets/search/" + _category + "?map=c"
	}).done(function (data) {
		var _trees = data.CategoriesTrees;
		for (var i = 0; i < _trees.length; i++) {
			var _name = tolowercase(_trees[i].Name);
			if (_category == _name) {
				var _html = '{{#each this}}<div class="thisCategory"><a href="{{#lowercase Link}}{{this}}{{/lowercase}}"><div class="image"><img src="/arquivos/{{#lowercase Name}}{{this}}{{/lowercase}}.jpg" alt="{{Name}}"></div><h2 class="text">{{Name}}</h2></a></div>{{/each}}';
				handlebarsjs(_html, _trees[i].Children, $("#category-list"), "html");
			}
		}
		slideShelf("#category-list", 7);
	});
}
// create sidekick
function createSidekick(data) {
	$("<ul />").addClass("items").appendTo("#sidekick");
	for (var i = 0; i < data.length; i++) {
		var name = data[i].name;
		name = name.replace(", ", " / ");
		$("<li />").html($("<a />").attr("href", data[i].link).html('<span class="icon">' + data[i].icon + '</span>' + '<p class="title">' + name + '</p>')).addClass(tolowercase(data[i].name)).addClass("item-" + (i + 1)).appendTo(".items");
	}
}
// loading banner
function banner(elemento) {
	$(elemento).slick({
		arrows: false,
		dots: true,
		autoplay: true
	});
}
// replace zero
function replaceZero(element) {
	$(element).each(function () {
		var _text = $(this).text(),
			_text = _text.split(",");
		$(this).text(_text[0]);
	});
}
// format money
function adjustMoneda(money) {
	var re = /\b(\d+)(\d{2})\b/;
	var str = money.toString();
	var subst = '$1.$2';
	var result = str.replace(re, subst);
	return result;
}
// update cart item
function updateCart($id, $qnt, $elemento) {
	vtexjs.checkout.getOrderForm().then(function (orderForm) {
		var itemIndex;
		var item = orderForm.items;
		for (var key in item) {
			if (item[key].id == $id) {
				var updateItem = {
					"index": key,
					"quantity": $qnt
				};
				return vtexjs.checkout.updateItems([updateItem]);
			}
		}
	}).done(function (orderForm) {
		refreshCart();
		minicart();
		mensagens(orderForm.messages);
		$($elemento).parent().parent().parent().parent().find(".loading").hide();
	});
}
// remove cart item
function removeCart($id, $elemento) {
	vtexjs.checkout.getOrderForm().then(function (orderForm) {
		var itemIndex;
		var item = orderForm.items;
		for (var key in item) {
			if (item[key].id == $id) {
				var itemsToRemove = {
					"index": key,
					"quantity": 0
				};
				return vtexjs.checkout.removeItems([itemsToRemove]);
			}
		}
	}).done(function (orderForm) {
		$($elemento).parent().parent().hide(0);
		refreshCart();
		minicart();
		$($elemento).parent().parent().find(".loading").hide();
	});
}
// carrito
function refreshCart() {
	var _commune = getCookie("commune");
	if (_commune != false) {
		vtexjs.checkout.getOrderForm().done(function (orderForm) {
			var qnt = 0;
			orderForm.items.forEach(function (e) {
				qnt = qnt + e.quantity;
			});
			$(".amount").text(qnt + " item(s)");
			$(".total").text(accounting.formatMoney(adjustMoneda(orderForm.value), "$ ", 0, ".", ","));
		});
	} else {
		return false;
	}
}
// comprar
function buyInAction() {
	$(document).on("click", ".buysku", function (e) {
		e.preventDefault();
		var _this = $(this);
		_this.parent().parent().parent().find(".loading").css("visibility", "visible");
		_this.find(".cargando").show(0);
		_this.find(".okay").hide(0);
		var _commune = getCookie("commune");
		if (_commune != false) {
			var _this = $(this);
			var id = this.getAttribute('data-id');
			var amount = Number(_this.prev().find("#amout").val());
			vtexjs.checkout.getOrderForm().then(function (orderForm) {
				orderForm.items.forEach(function (result) {
					if (result.id == id) {
						amount = result.quantity + amount;
					}
				});
				var item = [{
					id: id,
					quantity: amount,
					seller: 1
				}];
				return vtexjs.checkout.addToCart(item);
			}).done(function (orderForm) {
				_this.find(".cargando").hide(0);
				_this.find(".okay").show(0);
				_this.parent().parent().parent().find(".loading").css("visibility", "hidden");
				var add = $('<p class="add" />').text(" " + amount + " en el carro");
				_this.parent().parent().parent().find(".flags").append(add);
				add.delay(2000).fadeOut("slow");
				_this.parent().parent().parent().css("border-color", "rgb(0, 148, 94)");
				_this.parent().find(".qty").find("#amout").val("1");
				refreshCart();
				minicart();
				mensagens(orderForm.messages);
			});
		} else {
			$(".comunas-popup").css("visibility", "visible");
		}
	});
	$(document).on("click", ".counter .up", function (e) {
		var _this = $(this);
		var amount = Number(_this.parent().parent().find("#amout").val());
		if (amount != 48) {
			amount = Number(++amount);
			_this.parent().parent().find("#amout").val(amount);
		}
	});
	$(document).on("click", ".counter .down", function (e) {
		var _this = $(this);
		var amount = Number(_this.parent().parent().find("#amout").val());
		if (amount != 1) {
			amount = Number(--amount);
			_this.parent().parent().find("#amout").val(amount);
		}
	});
}
// informaçoes iniciais
function initialInfo(data, elemento, _idSKU) {
	var $json;
	data.forEach(function (jsonElement) {
		if (jsonElement.skuid == _idSKU) {
			var $elige = ("Elige " + jsonElement.selector[0].Atributo);
			var $price;
			// vtexjs.catalog.getProductWithVariations(Number(elemento.data("id"))).done(function (product) {
			// 	var _skus = product.skus;
			// 	_skus.forEach(function (skus) {
			// 		if (skus.sku == _idSKU) {

			// 		}
			// 	});
			// });

			var $price = elemento.find(".best-price").text(),
				$price = $price.replace("$ ", "").replace(".", "").split(","),
				$price = Number($price[0]);

			for (var l = 0; l < jsonElement.convertor.length; l++) {
				if (jsonElement.convertor[l].Atributo == "Factor de Conversión") {
					var $fator = Number(jsonElement.convertor[l].Valor.replace(",", "."));
				}
				if (jsonElement.convertor[l].Atributo == "Valor de Medida") {
					var $medida = Number(jsonElement.convertor[l].Valor.replace(",", "."));
				}
				if (jsonElement.convertor[l].Atributo == "Unidad de Referencia") {
					var $referencia = jsonElement.convertor[l].Valor;
				}
			}

			var $priceper = ($price / ($medida * $fator)),
				$priceper = accounting.formatMoney($priceper, "$ ", 0, ".", ","),
				$priceper = "<small>" + $priceper + " x " + $referencia + "</small>";

			$json = {
				id: jsonElement.skuid,
				elige: $elige,
				priceper: $priceper
			}
		}
	});
	return $json;
}
// skus on grid
function skuOnGrid() {
	var _height, __height = 0;
	$(".shelf ul li .container").each(function () {
		var _this = $(this);
		var _flags = _this.find(".flag");
		_flags.each(function (i, e) {
			var $class = $(e).attr("class");
			if ($class.indexOf("cyber") >= 0) {
				// _this.addClass("cybermonday");
			}
		});
		if (!_this.hasClass("done")) {
			try {
				_this.addClass("done");

				_this.find(".insert-sku-checklist li").each(function () {
					if ($(this).hasClass("unavailable")) {
						$(this).remove();
					}
				});

				var _idSKU = _this.find(".insert-sku-checklist li:first-child .insert-sku-checkbox").attr("name");
				_this.find(".insert-sku-checkbox[rel='" + _idSKU + "']").next().find(".insert-sku-name").addClass("active");
				$(_this.find(".insert-sku-checkbox[rel='" + _idSKU + "']").parent()).prependTo(_this.find(".insert-sku-checklist"));
				var _json = _this.find(".conversion-data .product-field ul li:first-child").text(),
					_json = $.parseJSON(_json),
					_json = _json.Sku;

				var _list = _this.find(".list-price s");
				var $bprice = _list.text(),
					$bprice = $bprice.split(","),
					$bprice = $bprice[0];
				_list.text($bprice);

				var _best = _this.find(".best-price");
				var $lprice = _best.text(),
					$lprice = $lprice.split(","),
					$lprice = $lprice[0];
				_best.text($lprice);

				__json = initialInfo(_json, _this, _idSKU);

				_this.find(".skuSelect h6").text(__json.elige);
				_this.find(".price-per").html(__json.priceper);
				_this.find(".buysku").attr("data-id", __json.id);

				_json.forEach(function (result) {
					var _dataJson = (JSON.stringify(result));
					_this.find(".insert-sku-checklist li .insert-sku-checkbox[rel='" + result.skuid + "']").next().find(".insert-sku-name").text(result.selector[0].Valor).attr("data-id", result.skuid).attr("data-info", _dataJson);
				});
			} catch (error) {

				// console.log(error);

				var _list = _this.find(".list-price s");
				var $bprice = _list.text(),
					$bprice = $bprice.split(","),
					$bprice = $bprice[0];
				_list.text($bprice);

				var _best = _this.find(".best-price");
				var $lprice = _best.text(),
					$lprice = $lprice.split(","),
					$lprice = $lprice[0];
				_best.text($lprice);

				_this.find(".variations").css("visibility", "hidden");
				_this.find(".buysku").attr("data-id", _this.find(".is-checklist-item:first-child .insert-sku-checkbox").attr("name"));
				_this.find(".loading").css("visibility", "hidden");
			}
		}
		_height = _this.find(".insert-sku-checklist").height();
		if (_height > __height) {
			__height = _height;
		}
	});
	$(".shelf ul li .container").find(".insert-sku-checklist").height(__height);
	$(".insert-sku-name").click(function (e) {
		e.preventDefault();
		var _this = $(this);
		var _dataInfo = _this.data("info");
		var _elemento = _this.parent().parent().parent().parent().parent().parent().parent().parent().parent().parent();
		var _loading = _elemento.find(".loading");
		var _productId = Number(_elemento.data("id"));
		var _skuId = _this.data("id");
		_this.parent().parent().parent().parent().parent().parent().parent().parent().parent().find(".buy .buysku").attr("data-id", _skuId);
		_this.parent().parent().parent().find(".is-checklist-item label .insert-sku-name").removeClass("active");
		_this.addClass("active");
		createSku(_productId, _skuId, _loading, _dataInfo, _elemento);
	});
}
// criando sku
function createSku(productId, skuId, loading, dataInfo, elemento) {
	loading.css("visibility", "visible");
	var __skus;

	vtexjs.catalog.getProductWithVariations(productId).then(function (product) {
		var _skus = product.skus;
		_skus.forEach(function (skus) {
			if (skus.sku == skuId) {
				$.extend(skus, dataInfo);
				__skus = skus;
			}
		});
		var $elige = ("Elige " + __skus.selector[0].Atributo);

		for (var l = 0; l < __skus.convertor.length; l++) {
			if (__skus.convertor[l].Atributo == "Factor de Conversión") {
				var $fator = Number(__skus.convertor[l].Valor.replace(",", "."));
			}
			if (__skus.convertor[l].Atributo == "Valor de Medida") {
				var $medida = Number(__skus.convertor[l].Valor.replace(",", "."));
			}
			if (__skus.convertor[l].Atributo == "Unidad de Referencia") {
				var $referencia = __skus.convertor[l].Valor;
			}
		}

		var $priceper = (__skus.bestPrice / ($medida * $fator)),
			$priceper = accounting.formatMoney(adjustMoneda($priceper), "$ ", 0, ".", ","),
			$priceper = "<small>" + $priceper + " x " + $referencia + "</small>";

		$json = {
			id: __skus.skuid,
			elige: $elige,
			priceper: $priceper,
			price: (__skus.listPrice > 0) ? '<span class="list-price"><s>' + accounting.formatMoney(adjustMoneda(__skus.listPrice), "$ ", 0, ".", ",") + '</s></span><span class="best-price">' + accounting.formatMoney(adjustMoneda(__skus.bestPrice), "$ ", 0, ".", ",") + '</span>' : '<span class="best-price">' + accounting.formatMoney(adjustMoneda(__skus.bestPrice), "$ ", 0, ".", ",") + '</span>',
			image: __skus.image.replace("-700-700", "-220-220")
		}

		skuConstructor($json, elemento);
	}).done(function () {
		loading.css("visibility", "hidden");
	});
}
// Construindo SKU
function skuConstructor(json, elemento) {
	elemento.find(".price-per").html(json.priceper);
	elemento.find(".price").html(json.price);
	elemento.find(".image a img").attr("src", json.image);
}
function mensagens(mens) {
	mens.forEach(function (msg) {
		var text = msg.text;
		var status = msg.status;
		status = (status == "warning") ? "warn" : status;
		$.notify(text, status, {
			clickToHide: false,
			autoHide: false,
			showDuration: 1000
		});
	});
	vtexjs.checkout.getOrderForm().then(function (orderForm) {
		return vtexjs.checkout.clearMessages();
	});
}
// product
function producto() {
	$.ajax({
		url: "/api/catalog_system/pub/products/search?fq=productId:" + skuJson_0.productId,
		success: function (data) {
			var elemento = $("#sku-info");
			var _data = data[0].items;
			var _json = data[0].ValoresGrilla[0],
				_json = JSON.parse(_json),
				_json = _json.Sku;
			var _first = null;
			// console.log(_json);
			_json.forEach(function (jsonElement, jsonIndex) {
				_data.forEach(function (dataElement, dataIndex) {
					if (dataElement.itemId == jsonElement.skuid) {
						if (dataElement.sellers[0].commertialOffer.AvailableQuantity > 0) {
							var $span = $("<span />").text(jsonElement.selector[0].Valor);
							var $elige = ("Elige " + jsonElement.selector[0].Atributo);
							if (dataElement.sellers[0].commertialOffer.Price == dataElement.sellers[0].commertialOffer.ListPrice) {
								var $price = dataElement.sellers[0].commertialOffer.ListPrice;
							} else {
								var $price = dataElement.sellers[0].commertialOffer.Price;
							}
							for (var l = 0; l < jsonElement.convertor.length; l++) {
								if (jsonElement.convertor[l].Atributo == "Factor de Conversión") {
									var fator = Number(jsonElement.convertor[l].Valor.replace(",", "."));
									// console.log(fator);
								}
								if (jsonElement.convertor[l].Atributo == "Valor de Medida") {
									var medida = Number(jsonElement.convertor[l].Valor.replace(",", "."));
								}
								if (jsonElement.convertor[l].Atributo == "Unidad de Referencia") {
									var referencia = jsonElement.convertor[l].Valor;
								}
							}
							var priceper = ($price / (medida * fator)),
								priceper = accounting.formatMoney(priceper, "$ ", 0, ".", ","),
								priceper = "<small>" + priceper + " x " + referencia + "</small>";
							var $json = {
								id: jsonElement.skuid,
								image: dataElement.images["0"].imageUrl.replace(dataElement.images["0"].imageId, dataElement.images["0"].imageId + "-550-550"),
								images: dataElement.images,
								price: dataElement.sellers[0].commertialOffer.Price,
								listprice: (dataElement.sellers[0].commertialOffer.Price == dataElement.sellers[0].commertialOffer.ListPrice) ? null : dataElement.sellers[0].commertialOffer.ListPrice,
								priceper: priceper,
								elige: $elige,
								elemento: elemento
							}
							$(elemento.find(".skuSelect").find(".variations")).append($span);
							$($span).unbind().click($json, function (e) {
								$(elemento).find(".skuSelect").find("h6").text($json.elige);
								$(elemento).find(".price-per").html($json.priceper);
								$(elemento).prev(".images").find(".image").find("a").find("img").attr("src", $json.image);
								if ($json.listprice == null) {
									$(elemento).find(".price").html('<span class="best-price">' + accounting.formatMoney($json.price, "$ ", 0, ".", ",") + '</span>')
								} else {
									$(elemento).find(".price").html('<span class="list-price"><s>' + accounting.formatMoney($json.listprice, "$ ", 0, ".", ",") + '</s></span><span class="best-price">' + accounting.formatMoney($json.price, "$ ", 0, ".", ",") + '</span>')
								}
								$(elemento).find(".buy").find(".buysku").attr("data-id", $json.id);
								elemento.find(".skuSelect").find(".variations").find("span:first-of-type").toggleClass("active").siblings().removeClass();
								$(this).addClass("active").siblings().removeClass("active");
								var $data = $json.images;
								$(".thumbs").html("");
								for (var i = 0; i < $data.length; i++) {
									if (i == 0) {
										$(".thumbs").append("<li class='active'>" + $data[i].imageTag.replace(/#width#/g, "100").replace(/#height#/g, "100").replace("~", "") + "</li>");
									} else {
										$(".thumbs").append("<li>" + $data[i].imageTag.replace(/#width#/g, "100").replace(/#height#/g, "100").replace("~", "") + "</li>");
									}
								}
							});
							if (_first == null) {
								$(elemento).find(".skuSelect").find("h6").text($json.elige);
								$(elemento).find(".price-per").html($json.priceper);
								$(elemento).prev(".images").find(".image").find("a").find("img").attr("src", $json.image);
								if ($json.listprice == null) {
									$(elemento).find(".price").html('<span class="best-price">' + accounting.formatMoney($json.price, "$ ", 0, ".", ",") + '</span>')
								} else {
									$(elemento).find(".price").html('<span class="list-price"><s>' + accounting.formatMoney($json.listprice, "$ ", 0, ".", ",") + '</s></span><span class="best-price">' + accounting.formatMoney($json.price, "$ ", 0, ".", ",") + '</span>')
								}
								$(elemento).find(".buy").find(".buysku").attr("data-id", $json.id);
								elemento.find(".skuSelect").find(".variations").find("span:first-of-type").addClass("active").siblings().removeClass();
								var $data = _data[0].images;
								for (var i = 0; i < $data.length; i++) {
									if (i == 0) {
										$(".thumbs").append("<li class='active'>" + $data[i].imageTag.replace(/#width#/g, "100").replace(/#height#/g, "100").replace("~", "") + "</li>");
									} else {
										$(".thumbs").append("<li>" + $data[i].imageTag.replace(/#width#/g, "100").replace(/#height#/g, "100").replace("~", "") + "</li>");
									}
								}
								_first = true;
							}
							$(document).on("click", ".thumbs li", function (e) {
								e.preventDefault();
								$(this).addClass("active").siblings().removeClass("active");
								var $img = $(this).find("img").attr("src"),
									$img = $img.replace("-100-100", "-500-500");
								$(".images .image a img").attr("src", $img);
							});
						}
					}
				});
			});
		}
	});
}
// slide vitrina
function slideShelf(elem, qty, dots) {
	if (dots == undefined) {
		dots = true;
	}
	$(elem).slick({
		dots: dots,
		infinite: true,
		speed: 600,
		nextArrow: '<button class="slick-right"><span><i class="fa fa-angle-right" aria-hidden="true"></i></span></button>',
		prevArrow: '<button class="slick-left"><span><i class="fa fa-angle-left" aria-hidden="true"></i></span></button>',
		cssEase: "cubic-bezier(.37,.64,.85,.98)",
		slidesToShow: qty,
		slidesToScroll: qty,
		responsive: [{
			breakpoint: 1440,
			settings: {
				slidesToShow: (qty - 1),
				slidesToScroll: (qty - 1)
			}
		}, {
			breakpoint: 1200,
			settings: {
				slidesToShow: (qty - 2),
				slidesToScroll: (qty - 2)
			}
		}, {
			breakpoint: 960,
			settings: {
				slidesToShow: (qty - 3),
				slidesToScroll: (qty - 3)
			}
		}, {
			breakpoint: 720,
			settings: {
				slidesToShow: (qty - 4),
				slidesToScroll: (qty - 4)
			}
		}]
	});
}
// background banner
function bkBanner(element) {
	$(element).each(function () {
		var $image = $(this).find("a").find("img");
		var $urlImage = $image.attr("src");
		var $link = $(this).find("a");
		$link.css({
			"background-image": "url('" + $urlImage + "')"
		});
		$image.remove();
	});
}
//filto Movil
function filterMobile() {
	$(".rb").on("click", function () {
		$(this).toggleClass("active");
		$(this).next("div").slideToggle("fast");
	});
}
// menu {
function hamburger() {
	$(".hamburger").click(function () {
		$("#menu").toggle(0);
		$(this).toggleClass("active");
	});
}
// ver mas category
function vermas() {
	$(".search-single-navigator ul").each(function () {
		var _this = $(this);
		if (_this.find("li").length > 4) {
			var _mas = $("<li />").html('Ver mas').addClass("vermas");
			var _menos = $("<li />").html('Ver menos').addClass("vermenos");
			$(_mas).insertAfter(_this.find("li:last-child"));
			$(_menos).insertAfter(_this.find("li:last-child"));
			$(_mas).unbind().click(function () {
				$(this).hide(0);
				$(this).parent().addClass("active");
				$(_menos).show(0);
			});
			$(_menos).unbind().click(function () {
				$(this).hide(0);
				$(_mas).show(0);
				$(this).parent().removeClass("active");
			});
		}
	});
}
// minicart
function minicart() {
	var _commune = getCookie("commune");
	if (_commune != false) {
		if ($("body").hasClass("home") || $("body").hasClass("departamento") || $("body").hasClass("institucional") || $("body").hasClass("recover-my-cart") || $("body").hasClass("consumo-hoy") || $("body").hasClass("verano") || $("body").hasClass("colaciones") || $("body").hasClass("maxcotas") || $("body").hasClass("product") || $("body").hasClass("search"))  {
			$("#cart").unbind().click(function () {
				$(this).toggleClass("activeHome");
				$("#minicart").fadeToggle();
			});
		}
		vtexjs.checkout.getOrderForm().done(function (orderForm) {
			var $html = '<ul class="items">{{reverse items}}{{#each items}}<li class="item"><div class="loading"><div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div></div><div class="img"><button class="delete" data-id="{{id}}"><i class="fa fa-times-circle" aria-hidden="true"></i></button><a href="{{detailUrl}}"><img src="{{imageUrl}}" alt="{{name}}" /></a></div><div class="name-price"><a class="name" href="{{detailUrl}}">{{name}}</a><a class="price" href="{{detailUrl}}">{{#pricing sellingPrice}}{{this}}{{/pricing}}</a></div><div class="buy"><div class="qty"><input type="text" name="amout" id="amout" value="{{quantity}}" maxlength="2"><div class="counter" data-id="{{id}}"><span class="up"><i class="fa fa-caret-up" aria-hidden="true"></i></span><span class="down"><i class="fa fa-caret-down" aria-hidden="true"></i></span></div></div></div></li>{{/each}}</ul><div class="buttons"><button class="save-cart"><span class="load-save"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i></span><span class="okay-save"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guarda tu carro</span></button><button class="go-cart"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Pagar</button></div>';
			if ($("body").hasClass("home") || $("body").hasClass("departamento") || $("body").hasClass("institucional") || $("body").hasClass("recover-my-cart") || $("body").hasClass("consumo-hoy") || $("body").hasClass("verano") || $("body").hasClass("colaciones") || $("body").hasClass("maxcotas") || $("body").hasClass("product") || $("body").hasClass("search")) {
				handlebarsjs($html, orderForm, "#minicart", "html");
				$('#minicart .items .item .loading').hide();
			} else {
				handlebarsjs($html, orderForm, "aside#cart", "html");
				$('aside#cart').sticky({ topSpacing: 20, bottomSpacing: 850 });
				$('#cart .items .item .loading').hide();
			}
			if (1 != 1) {
				$.ajax({
					url: "//api.vtex.com/elsuper/dataentities/CG/search?_fields=items&_where=email=" + vtexjs.checkout.orderForm.clientProfileData.email,
				}).done(function (result) {
					if (result.length > 0 && !getCookie("minicart")) {
						var _text = '<div class="recover-cart"><div class="loading" style="display: none;"><div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div></div><div class="container-recovery"><h3 class="title"><a href="#" id="recover-cart">¿Quieres recuperar tu carro guardado?</a></h3><div class="buttons"><button class="si">Sí</button><button class="no">No</button></div></div></div>';
						$(_text).prependTo(".save-cart-okay");
						$(".recover-cart .buttons .no").click(function () {
							$(".recover-cart").fadeOut();
							setCookie("minicart", true, 1);
						});
						$(".recover-cart .buttons .si").click(function () {
							$(".loading").fadeIn();
							vtexjs.checkout.getOrderForm().then(function (orderForm) {
								var item = result[0].items;
								return vtexjs.checkout.addToCart(item);
							}).done(function (orderForm) {
								refreshCart();
								minicart();
								$(".recover-cart").fadeOut();
								$(".loading").fadeOut();
								setCookie("minicart", true, 999);
								saveCart([], vtexjs.checkout.orderForm.clientProfileData.email, null);
							});
						});
					}
				}).error(function () {
					return false;
				});
			}

			login();

			$(".items--item__conteiner .price").each(function () {
				var $elemT = $(this).text();
				var $totalsT = $elemT.substring(0, ($elemT.length - 2));
				$(this).text(accounting.formatMoney($totalsT, "$ ", 0, ".", ","));
			});
			$(document).on("click", ".delete", function () {
				$(this).parent().parent().find(".loading").show(0);
				removeCart($(this).data("id"), $(this));
			});
			$(document).on("click", ".item .counter .up", function (e) {
				var _this = $(this);
				var _id = $(this).parent().data("id");
				var amount = Number(_this.parent().parent().find("#amout").val());
				_this.parent().parent().parent().parent().find(".loading").show();
				updateCart(_id, amount, _this);
			});
			$(document).on("click", ".item .counter .down", function (e) {
				var _this = $(this);
				var _id = $(this).parent().data("id");
				var amount = Number(_this.parent().parent().find("#amout").val());
				_this.parent().parent().parent().parent().find(".loading").show();
				updateCart(_id, amount, _this);
			});
			$(document).on("click", ".go-cart", function () {
				document.location = "/cart";
			});
			$(".save-cart").unbind().click(function (e) {
				e.preventDefault();
				var _this = $(this);
				_this.find(".okay-save").hide(0);
				_this.find(".load-save").show(0);
				vtexjs.checkout.getOrderForm().done(function (orderForm) {
					var _email = orderForm.clientProfileData.email;
					var __items = [];
					orderForm.items.forEach(function (r) {
						__items.push({
							id: r.id,
							quantity: r.quantity,
							seller: 1
						});
					});
					var _items = JSON.stringify(__items);
					saveCart(__items, _email, _this);
				});
			});
		});
	} else {
		return false;
	}
}
// save cart
function saveCart(items, email, elm) {
	if (elm == null) elm = $(".buttons");
	var datos = {};
	datos.idcart = null;
	datos.items = items;
	datos.total = null;
	datos.email = email;

	$.ajax({
		headers: {
			"Accept": "application/json; charset=utf-8",
			"Content-Type": "application/json; charset=utf-8"
		},
		data: JSON.stringify(datos),
		type: 'PATCH',
		url: '//api.vtexcrm.com.br/elsuper/dataentities/CG/documents',
		success: function (data) {
			elm.find(".load-save").hide(0);
			elm.find(".okay-save").show(0);
			$.notify("Tu carro fue grabado!", "success");
		},
		error: function (error) {
			var json = jQuery.parseJSON(error.responseText);
			var $error = json.ExceptionMessage;
			// console.log($error);
		}
	});
}
// avanzados category
function advancedCategory() {
	var _count = $(".searchResultsTime:first-of-type .resultado-busca-numero .value").text();
	var _text = _count + " productos encontrados";
	var _h3 = $(".search-single-navigator > h4");
	var _resultados = $("<span />").text(_text).addClass("resultados");
	var _ordenar = $("<span />").text("Ordenar productos").addClass("ordenar");
	var _orderbox = $("<div />").addClass("orderbox");
	var _orderby = '<div class="selecionado"><span class="elegido">Mejor descuento</span><ul class="orderby"><li class="mejor-dto selected" data-order="OrderByBestDiscountDESC">Mejor descuento</li><li class="mayor-precio" data-order="OrderByPriceDESC">Mayor precio</li><li class="menor-precio" data-order="OrderByPriceASC">Menor precio</li></ul></div><div class="gridlist"><span class="grid active"><i class="fa fa-th" aria-hidden="true"></i></span><span class="list"><i class="fa fa-th-list" aria-hidden="true"></i></span></div>'
	var _h6 = $('<h6 />').addClass("categorias").text("Categorías");
	if (_h3.next("ul").length > 0) var _true = true;
	$(_resultados).insertAfter(_h3);
	$(_ordenar).insertAfter(".resultados");
	$(_orderbox).insertAfter(".ordenar");
	$(_orderby).appendTo(".orderbox");
	if (_true) $(_h6).insertAfter(".orderbox");
	$(".gridlist .list").unbind().click(function () {
		$("[id^=ResultItems_] .shelf").removeClass("grid").addClass("list");
		$("[id^=ResultItems_]").removeClass("grid").addClass("list");
		$(this).addClass("active").siblings().removeClass("active");
	});
	$(".gridlist .grid").unbind().click(function () {
		$("[id^=ResultItems_] .shelf").removeClass("list").addClass("grid");
		$("[id^=ResultItems_]").removeClass("list").addClass("grid");
		$(this).addClass("active").siblings().removeClass("active");
	});
	if ($(".filtro-ativo").length >= 0) {
		$(".filtro-ativo").parent().addClass("selected");
	}
}
// communaPopup
function popupComuna() {
	$(".comunas-popup").css("visibility", "visible");
}
// franja horaria
function franjaClick() {
	// franja();
	$(document).on("click", ".compra-hoy-recibe-hoy a", function (e) {
		e.preventDefault();
		var _commune = getCookie("commune");
		if (_commune != false) {
			franja();
		} else {
			popupComuna();
		}
	});
	$(".franja-box .top .close").unbind().click(function (e) {
		e.preventDefault();
		$(".franja-container").css("visibility", "hidden");
		$(".franja-container .franja-box .content-container").css("visibility", "hidden");
	});
	$(".franja-container").unbind().click(function (e) {
		e.preventDefault();
		$(this).css("visibility", "hidden");
		$(".franja-container .franja-box .content-container").css("visibility", "hidden");
	});
}

function franja() {
	// $(".content:nth-of-type(2) a").click(function (e) {
	// 	e.preventDefault();
	// 	$(".browsering").css("visibility", "visible");
	// });
	// $(".browsering .generical .title .close").click(function (e) {
	// 	e.preventDefault();
	// 	$(".browsering").css("visibility", "hidden");
	// });

	// $(".franja-container").css("visibility", "visible");
	// var __html = '<div class="franja-box"><div class="loading"><div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div></div><div class="content-container"><div class="top"><h3>Horario de Despacho</h3><span class="close"><i class="fa fa-times-circle" aria-hidden="true"></i></span></div><p class="info">Selecciona el horario de despacho que más te acomode.</p><div class="table"><table id="franja"><thead><tr id="mes"><th class="title">Horarios</th></tr></thead><tbody><tr id="hora-1"><td class="hora-title">08:00 - 10:00</td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td></tr><tr id="hora-2"><td class="hora-title">10:00 - 12:00</td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td></tr><tr id="hora-3"><td class="hora-title">12:00 - 14:00</td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td></tr><tr id="hora-4"><td class="hora-title">14:00 - 16:00</td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td></tr><tr id="hora-5"><td class="hora-title">16:00 - 18:00</td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td></tr><tr id="hora-6"><td class="hora-title">18:00 - 20:00</td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td></tr><tr id="hora-7"><td class="hora-title">20:00 - 22:00</td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td><td class="status no-delivery"> --- </td></tr></tbody></table></div></div></div>'
	// $(".franja-container").html(__html);
	// var $franja = getCookie("franja");
	// var $commune = getCookie("commune");
	// var $zip = getCookie("communeZIP");
	// var items = [{
	// 	id: 1524,
	// 	quantity: 1,
	// 	seller: '1'
	// }];
	// var postalCode = ($zip == "") ? '7550000' : $zip;
	// var country = 'CHL';
	// vtexjs.checkout.simulateShipping(items, postalCode, country).then(function (orderForm) {
	// 	var _json = orderForm.logisticsInfo[0].slas[0].availableDeliveryWindows;
	// 	// console.log(orderForm.logisticsInfo[0].slas[0].availableDeliveryWindows);
	// 	var monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Deciembre"];
	// 	var weekNames = ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"];
	// 	var indice = 0;
	// 	_json.forEach(function (element, index) {
	// 		var startDateUtc = new Date(element.endDateUtc),
	// 			startDateUtc = startDateUtc.setTime(startDateUtc.getTime() + startDateUtc.getTimezoneOffset() * 60 * 1000),
	// 			startDateUtc = new Date(startDateUtc);
	// 		var day = startDateUtc.getDate();
	// 		var hours = startDateUtc.getHours();
	// 		var week = startDateUtc.getDay(),
	// 			week = weekNames[week];
	// 		var month = (startDateUtc.getMonth()),
	// 			month = monthNames[month];
	// 		var price = element.price;
	// 		var lastString = price.toString().substring(0, price.toString().length - 2);
	// 		var string = price.toString().substring(price.toString().length - 2);
	// 		price = (Number(lastString + "." + string));
	// 		price = accounting.formatMoney(price, "$ ", 0, ".", ",");
	// 		var _class = tolowercase(week + "-" + day + "-" + month).toString();
	// 		if ($("th." + _class).length == 0 && indice <= 6) {
	// 			++indice;
	// 			var _th = $("<th />").addClass(_class).html("<div class='week'>" + week + "</div><div class='mes'>" + day + " " + month + "</div>");
	// 			$(_th).appendTo("tr#mes");
	// 			addHours(hours, price, indice, startDateUtc.getDay(), startDateUtc.getDate());
	// 		} else if ($("th." + _class).length != 0) {
	// 			addHours(hours, price, indice, startDateUtc.getDay(), startDateUtc.getDate());
	// 		}
	// 	});
	// }).done(function (orderForm) {
	// 	$(".franja-container .franja-box .loading").css("visibility", "hidden");
	// 	$(".franja-container .franja-box .content-container").css("visibility", "visible");
	// 	setCookie("franja", $commune, 999);
	// });
}
// add hour
function addHours(hours, price, indice, day, date) {
	$.ajax({
		url: "//api.vtex.com/elsuper/dataentities/ST/search?_fields=timeLeft&_where=day=" + day + " AND closingTime=" + hours + ":00"
	}).done(function (result) {
		var minus = Number(hours - result[0].timeLeft);
		var dataAtual = new Date();
		var diaAtual = dataAtual.getDate();
		var horaAtual = dataAtual.getHours();

		if (diaAtual == date) {
			if (horaAtual < minus) {
				if (hours == 10) {
					$("#hora-1 td").eq(indice).text(price).addClass("delivery").addClass("date-" + date).removeClass("no-delivery");
				} else if (hours == 12) {
					$("#hora-2 td").eq(indice).text(price).addClass("delivery").addClass("date-" + date).removeClass("no-delivery");
				} else if (hours == 14) {
					$("#hora-3 td").eq(indice).text(price).addClass("delivery").addClass("date-" + date).removeClass("no-delivery");
				} else if (hours == 16) {
					$("#hora-4 td").eq(indice).text(price).addClass("delivery").addClass("date-" + date).removeClass("no-delivery");
				} else if (hours == 18) {
					$("#hora-5 td").eq(indice).text(price).addClass("delivery").addClass("date-" + date).removeClass("no-delivery");
				} else if (hours == 20) {
					$("#hora-6 td").eq(indice).text(price).addClass("delivery").addClass("date-" + date).removeClass("no-delivery");
				} else if (hours == 22) {
					$("#hora-7 td").eq(indice).text(price).addClass("delivery").addClass("date-" + date).removeClass("no-delivery");
				}
			}
		} else {
			if (hours == 10) {
				$("#hora-1 td").eq(indice).text(price).addClass("delivery").addClass("date-" + date).removeClass("no-delivery");
			} else if (hours == 12) {
				$("#hora-2 td").eq(indice).text(price).addClass("delivery").addClass("date-" + date).removeClass("no-delivery");
			} else if (hours == 14) {
				$("#hora-3 td").eq(indice).text(price).addClass("delivery").addClass("date-" + date).removeClass("no-delivery");
			} else if (hours == 16) {
				$("#hora-4 td").eq(indice).text(price).addClass("delivery").addClass("date-" + date).removeClass("no-delivery");
			} else if (hours == 18) {
				$("#hora-5 td").eq(indice).text(price).addClass("delivery").addClass("date-" + date).removeClass("no-delivery");
			} else if (hours == 20) {
				$("#hora-6 td").eq(indice).text(price).addClass("delivery").addClass("date-" + date).removeClass("no-delivery");
			} else if (hours == 22) {
				$("#hora-7 td").eq(indice).text(price).addClass("delivery").addClass("date-" + date).removeClass("no-delivery");
			}
		}
	});
}
// scrollGrind
function scrollOnGrid() {
	// $(".pages li").click(function () { $('html, body').animate({ scrollTop: $("body").offset().top }, 1000); });
	$(".shelf[id*=ResultItems]").QD_infinityScroll({
		elemLoading: '<div class="lodiando"><div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div></div>',
		returnToTop: $('<span />'),
		callback: function () {
			$(".helperComplement").remove();
			setTimeout(function () {
				skuOnGrid();
			}, 1000);
		},
		insertContent: function (currentItems, ajaxData) {
			var _html = $.parseHTML(ajaxData),
				_html = $(_html[1]).find("ul").html();
			$(currentItems).find(">ul").append(_html);
		}
	});
}
// login
function login() {
	var timerId = 0;
	timerId = setInterval(function () {
		if ($('[href="/no-cache/user/logout"]').length > 0) {
			$(".welcome").addClass("logueado");

			if ($("body").hasClass("home") || $("body").hasClass("departamento")) {
				if ($("#minicart .items *").length > 0) {
					$("#minicart").addClass("save-cart-okay");
				}
			}
			if ($("body").hasClass("category")) {
				if ($("aside#cart .items *").length > 0) {
					$("aside#cart").addClass("save-cart-okay");
				}
			}

			$(".logueado").unbind().click(function (e) {
				e.preventDefault();
				$(".logado").toggle();
				$("#minicart").hide(0);
			});
			clearInterval(timerId);
		}
	}, 1000);
}
// Desktop menu
if ($("body").hasClass("home")) {
	$(".menu #menu").show();
}

// mobile menu
function mobileMenu() {
	$("#mobile-menu").click(function () {
		$("#menu").toggleClass('active');
		$("body").toggleClass('active');
		$(this).toggleClass("active");
	});

	if (window.innerWidth <= 1200) {
		$("#menu .item").on("click", ".itemlink", function (event) {
			event.preventDefault();
			$(this).next(".submenu").toggleClass("active");
		})

		$("#menu .item").each(function () {
			var submenu = $(this).find(".submenu");
			submenu.prepend('<li class="subitem volver-item"><a href="##" class="volver">Volver</a></li>');
			var volver = submenu.find(".volver");

			volver.on("click", function (event) {
				event.preventDefault();
				$(this).offsetParent().removeClass("active")
			})
		});
	}

}
// shipping data send
function sendShipping() {
	vtexjs.checkout.getOrderForm().then(function (orderForm) {
		var shippingData = orderForm.shippingData;
		if (!shippingData) shippingData = {};
		if (!shippingData.logisticsInfo[0].slas[0].deliveryWindow) shippingData.logisticsInfo[0].slas[0].deliveryWindow = {};
		$.extend(shippingData.logisticsInfo[0].slas[0].deliveryWindow, {
			endDateUtc: "2017-08-02T16:00:00+00:00",
			lisPrice: 330000,
			price: 330000,
			startDateUtc: "2017-08-02T14:00:00+00:00",
			tax: 0
		});
		return vtexjs.checkout.sendAttachment('shippingData', shippingData);
	})
}
// link home h2
function verMais() {
	$(".shelf h2").each(function () {
		var _this = $(this);
		var _split = _this.text().split(";");
		if (_split.length > 1) {
			_this.html("<span class='titulo'>" + _split[0] + "</span><a class='link-titulo' href='" + _split[1] + "'>Ver más</a>");
		}
	});
}
// orderby
function orderBy() {
	var _href = window.location.href;
	var _elegido = $(".elegido");

	if (_href.indexOf("O=") >= 0) {
		if (_href.indexOf("OrderByBestDiscountDESC") >= 0) {
			$(".elegido").text("Mejor descuento");
		}
		if (_href.indexOf("OrderByPriceDESC") >= 0) {
			$(".elegido").text("Mayor precio");
		}
		if (_href.indexOf("OrderByPriceASC") >= 0) {
			$(".elegido").text("Menor precio");
		}
	}

	$(document).on("click", ".selecionado", function () {
		$(this).find(".orderby").toggle();
	});

	$(document).on("click", ".orderby li", function () {
		var _data = $(this).data("order");
		if (_href.indexOf("?") >= 0) {
			if (_href.indexOf("O=") >= 0) {
				_href = _href.replace("OrderByBestDiscountDESC", _data).replace("OrderByPriceDESC", _data).replace("OrderByPriceASC", _data);
			} else {
				_href = _href + "&O=" + _data;
			}
		} else {
			_href = _href + "?O=" + _data;
		}
		$(this).addClass("selected").siblings().removeClass("selected");
		$(".elegido").text($(this).text());
		window.location.href = _href;
	});
}

function sendFormCFYV() {
	$(".box button").on("click", function (e) {
		// avoid(0);
		var datos = {};
		datos.caja = $("[name='caja']:checked").val();
		datos.delivery = $("[name='semana']:checked").val();
		datos.email = $("#email").val();
		datos.nombre = $("#nombre").val();
		datos.telefono = $("#telefono").val();
		console.log(JSON.stringify(datos));
		$.ajax({
			headers: {
				"Accept": "application/json; charset=utf-8",
				"Content-Type": "application/json; charset=utf-8"
			},
			data: JSON.stringify(datos),
			type: 'PATCH',
			url: '//api.vtexcrm.com.br/elsuper/dataentities/FV/documents',
			success: function (data) {
				$(".form").html("<h2>!Inscripcion fue guardada! - Te llamamos para acordar el horario.</h2>");
			},
			error: function (error) {
				var $json = JSON.parse(error.responseText);
				var $error = $json.Message;
				switch ($error) {
					case "O valor '' informado para o campo 'E-mail' é inválido. O valor não é um e-mail.":
						$("#email").css("border-color", "red");
						break;
					case "O campo 'Telefono' deve ser preenchido.":
						$("#telefono").css("border-color", "red");
						break;
					default:
						$("#email").css("border-color", "red");
						break;
				}
			}
		});
	});
}
function sendFormVerano() {
	$(".box button").on("click", function (e) {
		// avoid(0);
		var datos = {};
		datos.data = $("#semana_1").val() + ", " + $("#semana_2").val() + ", " + $("#semana_3").val();
		datos.lugar = $("#vaciones_1").val() + ", " + $("#vaciones_2").val() + ", " + $("#vaciones_3").val();
		datos.email = $("#email").val();
		datos.telefono = $("#telefono").val();
		console.log(JSON.stringify(datos));
		$.ajax({
			headers: {
				"Accept": "application/json; charset=utf-8",
				"Content-Type": "application/json; charset=utf-8"
			},
			data: JSON.stringify(datos),
			type: 'PATCH',
			url: '//api.vtexcrm.com.br/elsuper/dataentities/IV/documents',
			success: function (data) {
				$(".form").html("<h2>!Inscripcion fue guardada! - Te llamamos para acordar el horario.</h2>");
			},
			error: function (error) {
				var $json = JSON.parse(error.responseText);
				var $error = $json.Message;
				switch ($error) {
					case "O valor '' informado para o campo 'E-mail' é inválido. O valor não é um e-mail.":
						$("#email").css("border-color", "red");
						break;
					case "O campo 'Telefono' deve ser preenchido.":
						$("#telefono").css("border-color", "red");
						break;
					default:
						$("#email").css("border-color", "red");
						break;
				}
			}
		});
	});
}

function saveList() {
	vtexjs.checkout.getOrderForm().done(function (orderForm) {
		var email = orderForm.clientProfileData.email;
		if (email !== null) {
			var TokenUsuario = getCookie("VtexIdclientAutCookie_elsuper");
			var items = orderForm.items;
			var nombrelista = "Mi Lista" + Math.floor((Math.random() * 10) + 1);
			var json = { "Nome": nombrelista, "Produtos": [], "TokenUsuario": TokenUsuario }

			$.each(items, function (i, data) {
				json.Produtos.push({ "IdVtexSku": data.id, "Quantidade": data.quantity });
			});
			console.log(json);

			$.ajax({
				headers: {
					"x-hubin-chef-api-appkey": "elsuperapi",
					"x-hubin-chef-api-apptoken": "HTJK571CD8B03AAD201A6BBA455B93CAAD0ODHT",
					"Content-Type": "application/json"
				},
				data: JSON.stringify(json),
				type: "POST",
				url: "https://elsuper.hubin.io/chef/api/listacompra/listausuariosalvar",
				success: function (data) {
					console.log("Data:");
					console.log(data);
				},
				error: function (error) {
					console.log("Error:");
					console.log(error);
				}
			});
		}
	});
}

$(function () {
	if ($("body").hasClass("cart")) {
		saveList();
	}
	global();
	// menu();
	sidekick();
	skuOnGrid();
	buscador();
	buyInAction();
	refreshCart();
	minicart();
	franjaClick();
	mobileMenu();
	newsletter();
	login();
	menuDestacado("238", ".productos-nuevos .sub-menu", "Productos Nuevos");
	menuDestacado("257", ".sin-gluten .sub-menu", "Libre de Gluten");
	menuDestacado("221", ".ofertas .subitem:nth-child(2) .sub-menu", "Super precios");
	menuDestacado("229", ".corta-duracion .sub-menu", "Corta duración");
	menuDestacado("216", ".colacion .sub-menu", "Productos para Colación");
	menuDestacado("327", ".327 .sub-menu", "Librería");
	// modalDelivery();
	franja();
	communa();

	if ($("body").width() >= 1115) {
		$(".item").hover(function () {
			$(this).find(".submenu").show();
		}, function () {
			$(this).find(".submenu").hide();
		});
	}

	// $(".subitem").hover(function () {
	// 	$(this).find(".sub-menu").show();
	// }, function () {
	// 	$(this).find(".sub-menu").hide();
	// });

	if ($("body").hasClass("home")) {
		if ($("body").width() <= 1200) {
			$("#menu").hide(0);
		} else {
			$("#menu").show(0);
		}
		verMais();
	}

	if ($("body").hasClass("departamento") || $("body").hasClass("category") || $("body").hasClass("search") || $("body").hasClass("brand")) {
		filterMobile();
		hamburger();
		noheadernumbers();
		vermas();
		scrollOnGrid();
	}
	if ($("body").hasClass("institucional") || $("body").hasClass("recover-my-cart")) {
		hamburger();
	}
	if ($("body").hasClass("departamento")) {
		$("[id^=ResultItems_]").removeClass("list").addClass("grid");
		createDepartmentChildren();
	}
	if ($("body").hasClass("category")) {
		advancedCategory();
		orderBy();
	}

	if ($("body").hasClass("recover-my-cart")) {
		clickRecupero();
	}
	if ($("body").hasClass("colaciones") || $("body").hasClass("maxcotas") || $("body").hasClass("navidad") || $("body").hasClass("ano-nuevo") || $("body").hasClass("enamorados") || $("body").hasClass("consumo-hoy")) {
		$("<span id='topo'><i class='fa fa-arrow-up' aria-hidden='true'></i></span>").appendTo("body");
		hamburger();
		verMais();
		$("#topo").click(function () {
			$('html, body').animate({
				scrollTop: $(".banner").offset().top
			}, "slow");
			return false;
		});
		$(".thisCategory").click(function () {
			var $this = $(this);
			var $id = $this.find("a").attr("href");
			$('html, body').animate({
				scrollTop: $($id).offset().top
			}, "slow");
		});
	}
	if ($("body").hasClass("product")) {
		hamburger();
		producto();
	}
});

function maxcotas() {
	$("#has-category .box-banner").each(function (i, e) {
		var $elem = $(this).find("a");
		var $alt = $elem.find("img").attr("alt");
		var $h3 = $("<h3 />").text($alt).addClass("title");
		$($elem).append($h3);
	});
	$("<span id='topo'><i class='fa fa-arrow-up' aria-hidden='true'></i></span>").appendTo("body");
	$("#topo").click(function () {
		$('html, body').animate({
			scrollTop: $("#has-category").offset().top
		}, "slow");
		return false;
	});
	// document.title = $("#title-meta").text();
}

function pascua() {
	$("#has-category .box-banner").each(function (i, e) {
		var $elem = $(this).find("a");
		var $alt = $elem.find("img").attr("alt");
		var $h3 = $("<h3 />").text($alt).addClass("title");
		$($elem).append($h3);
	});
}

function mislistas() {
	var TokenUsuario = getCookie("VtexIdclientAutCookie_elsuper");
	$.ajax({
		headers: {
			"Content-Type": "application/json"
		},
		type: "GET",
		url: "http://elsuper.hubin.io/chef/api/listacompra/listausuario?sellerId=1&tokenusuario=" + TokenUsuario,
		success: function (data) {
			listasLoadData(data, TokenUsuario);
		},
		complete: function() {
			$(".elsuper-loading").hide();
		},
		error: function (error) {
			console.log(error);
		}
	});
}

function listasLoadData(data, TokenUsuario) {
	var listData = $.makeArray(data);
	var token = TokenUsuario;

	$.map(listData, function(key, value) {
		var lists = key['Results'];
		var listsNumber;
		if (lists.length == 1) {
			listsNumber = "(1 lista)";
		} else {
			listsNumber = "(" + lists.length + " listas)";
		}

		$("<span class='lists'>" + listsNumber + "</span>").appendTo(".mis-listas");

		if (lists.length > 0) {
			$(lists).each(function(key){
				var listId = lists[key]['IdLista'];
				var listName = lists[key]['Nome'];
				var listLink = lists[key]['Links']['DetalhesDaLista'] + token;
				var products = lists[key]['Produtos'];
				var listValue = lists[key]['ValorTotalListaToShow'];
				var listSize;
				
				if (products.length > 0 ) {
					listSize = products.length + " itens";
				} else {
					listSize = "vazía"
				}
				$("<li class='list list-" + listId +"'><div class='list-name'><span class='open-list'>" + listName + "<span class='list-size'>(" + listSize + ")</span></span></div><div class='list-details'><span class='list-value'>Valor total: " + listValue + "</span><span class='btn btn-see-list open-list'>Ver Lista</span><span class='btn btn-exclude-list'>Excluir <i class='fa fa-times-circle'></i></span></div></li>").appendTo(".user-lists");
				if (products.length > 0) {
					$("<ul class='list-" + listId + "-products'></ul>").appendTo(".list-" + listId);
					$("<li class='list-titles'><span class='products-name-column'>Productos</span><span class='products-description-column'>Descriptión</span><span class='products-number-column'>Cantidad</span><span class='products-value-column'>Valor</span></li>").appendTo(".list-" + listId + "-products");
					$(products).each(function(productKey){
						var productId = products[productKey]['VtexSku']['Id'];
						var productName = products[productKey]['VtexSku']['Name'];
						var productDescription = "";
						var prodcutNumber = products[productKey]['QtdIngrediente'];
						var productUnitPrice = products[productKey]['UnitPrice'];
						var productMeasurement = products[productKey]['VtexSku']['MeasurementUnit'];

						if (products[productKey]['VtexSku']['Descricao'] != null ) {
							productDescription = products[productKey]['VtexSku']['Descricao'];
						}

						$("<li class=product-" + productId +"><span class='product-name'>" + productName + "</span><span class='product-description'>" + productDescription +"</span><span class='product-number'>"+ prodcutNumber +"</span><span class='product-unit-value'>" + productUnitPrice + "<span class='product-measurement'>" + productMeasurement + "</span></span><i class='fa fa-times-circle'></i></li>").appendTo(".list-" + listId + "-products");
					});
				} else {
					$("<ul class='empty-list'><li>Esta lista está vacía</li></ul>").appendTo(".list-" + listId);
				}
			});
		}
	});

	$(".open-list").click(function(){
		$(this).parent().parent().find("ul").toggle();
	});

	excludeProductLista();
}

function excludeProductLista() {
	$(".fa-times-circle").click(function(){
		$(this).parent().remove();
	});
}

function saveMisListas() {
	var Token = getCookie("VtexIdclientAutCookie_elsuper");
	var listName = $('.new-list-name').val();
	var listSettings = { 
			"idListaSubCategoria": "",
			"Nome": listName,
			"Descricao": "",
			"PalavraChave": "",
			"Produtos": [{}],
			"TokenUsuario": Token
		};
	$.ajax({
		headers: {
			"Content-Type": "application/json"
		},
		type: "POST",
		url: "http://elsuper.hubin.io/chef/api/listacompra/listausuariosalvar",
		processData: false,
		data: JSON.stringify(listSettings),
		success: function (data) {
			console.log(data);
		},
		complete: function() {
			location.reload();
		},
		error: function (error) {
			console.log(error);
		}
	});
}

function listPopUp() {
	$('.new-list-popup').popup({
	  transition: 'all 0.3s',
	  openelement: '.list-button',
	  // closeelement: '.btn-exit',
	  focuselement: '.new-list-name'
	});

	$('.btn-save').click(function(){
		saveMisListas();
	});
}

// ready jquery
$(document).on("ready", function () {
	listPopUp();
	if ($("body").hasClass("my-lists")) {
		mislistas();
	}
	if ($("body").hasClass("home")) {
		banner("#banner");
		slideShelf(".shelf >ul", 5);
	}
	if ($("body").hasClass("navidad") || $("body").hasClass("ano-nuevo") || $("body").hasClass("enamorados") || $("body").hasClass("consumo-hoy")) {
		slideShelf(".shelf >ul", 5);
	}

	if ($("body").hasClass("maxcotas")) {
		slideShelf(".shelf > ul", 5);
		maxcotas();
		slideShelf("#has-category", 5, false);
	}
	if ($("body").hasClass("cybermonday")) {
		noTitle();
	}
	if ($("body").hasClass("pascua")) {
		pascua();
		verMais();
	}
	if ($("body").hasClass("product")) {
		slideShelf(".shelf >ul", 5);
	}
	if ($("body").hasClass("departamento")) {
		// slideShelf(".shelf ul", 5);
	}
	if ($("body").hasClass("libre-de-gluten")) {
		document.title = "Productos Libre de Gluten | elSuper.cl"
	}
	if ($("body").hasClass("caja-fruta-y-verdura")) {
		sendFormCFYV();
	}
	if ($("body").hasClass("verano")) {
		sendFormVerano();
	}
	if ($("body").hasClass("enamorados")) {
		verMais();
		slideShelf("#category-list", 5, false);
	}
	if ($("body").hasClass("colaciones")) {
		verMais();
		$("#has-category .box-banner").each(function () {
			var $this = $(this).find("a")
			var text = $this.find("img").attr("alt");
			var $h4 = $("<h4 />").addClass("title").text(text);
			$this.append($h4);
		});
		$("#has-category .box-banner").click(function () {
			var index = $(this).index();
			index = (index + 1);
			$('html, body').animate({
				scrollTop: $(".shelf:nth-child(" + index + ")").offset().top
			}, "slow");
		});
		slideShelf(".shelf >ul", 6);
		$('#has-category').slick({
			dots: false,
			infinite: true,
			speed: 600,
			nextArrow: '<button class="slick-right"><span><i class="fa fa-angle-right" aria-hidden="true"></i></span></button>',
			prevArrow: '<button class="slick-left"><span><i class="fa fa-angle-left" aria-hidden="true"></i></span></button>',
			cssEase: "cubic-bezier(.37,.64,.85,.98)",
			slidesToShow: 6,
			slidesToScroll: 6,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 5,
						slidesToScroll: 5,
					}
				},
				{
					breakpoint: 1024,
					settings: {
						slidesToShow: 4,
						slidesToScroll: 4,
					}
				},
				{
					breakpoint: 600,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 3
					}
				},
				{
					breakpoint: 480,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2
					}
				}
			]
		});
	}
});
// window resize
window.onresize = function (event) {
	if ($("body").hasClass("home")) {
		if ($("body").width() <= 1200) {
			$("#menu").hide(0);
		} else {
			$("#menu").show(0);
		}
	}
};
// ajax start
$(document).ajaxStart(function () { });
// ajax stop
$(document).ajaxStop(function () {
	if (!$("body").hasClass("home") || !$("body").hasClass("product")) {
		// setTimeout(skuOnGrid, 1000);
	}
});
// ajax complete
$(document).ajaxComplete(function () { });
// console.log("version 3.5.1");