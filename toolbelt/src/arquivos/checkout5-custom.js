var urlVtexMaster = "https://api.vtexcrm.com.br/elsuper/dataentities/CF/documents";
var urlVtexMasterSearch = "https://api.vtexcrm.com.br/elsuper/dataentities/CF/search?_where=PFAC_EMAIL={0}&_fields=id,PFAC_ORDERFORMID,PFAC_NROPEDIDO,PFAC_COMUNA,PFAC_DEPTO,PFAC_DIG,PFAC_DIRECCION,PFAC_GIRO,PFAC_EMAIL,PFAC_NUMERO,PFAC_RAZONSOCIAL,PFAC_REGION,PFAC_RUT,PFAC_TELEFONO1,PFAC_TELEFONO2,PFAC_NOMBREFANTASIA,PFAC_NOMBRECONTACTO";

var $checkBoxFactura = $('<div id="divChkCompraFactura" class="bottom-content"><label class="button golden" for="chkCompraFactura"><input id="chkCompraFactura" type="checkbox" /><span>Compra con Factura</span></label></div><br><div class="formFactura"></div>');
var $checkBoxFactura2 = $('<div id="divChkCompraFactura" class="span6 pull-right factura-data"><div class="step accordion-group factura-data active"><div class="accordion-heading"><span class="accordion-toggle collapsed accordion-factura-title"><span>Compra Con Factura</span><label class="button golden" for="chkCompraFactura"><input id="chkCompraFactura" type="checkbox" /><span data-i18n="shipping.title">Quiero Comprar con Factura.</span></label></div><br><div class="formFactura"></div></div></div>');
var $htmlFormulario = $("<div id='divFormCompraFactura' class='wrapFormFactura'><form id='formCompraFactura'><div class='rut'><span>Rut</span><input type='text' id='txtRut'><span id='spnErrorRut' class='error'></span></div><div class='razonSocial'><span>Rázon Social</span><input type='text' id='txtRazonSocial' maxlength='50'><span id='spnErrorRazonSocial' class='error'></span></div><div class='giro'><span>Giro</span><input type='text' id='txtGiro' maxlength='50'><span id='spnErrorGiro' class='error'></span></div><div class='nombreFantasia'><span>Nombre Fantasía</span><input type='text' id='txtNombreFantasia' maxlength='100'><span id='spnErrorNombreFantasia' class='error'></span></div><div class='direccion'><span>Dirección</span><input type='text' id='txtDireccion' maxlength='100'><span id='spnErrorDireccion' class='error'></span></div><div class='numero'><span>Numero</span><input type='text' id='txtNumero' maxlength='10'><span id='spnErrorNumero' class='error'></span></div><div class='numero'><span>Numero Depto.</span><input type='text' id='txtNumeroDepto' maxlength='10'></div><div class='region'><span>Región</span><select id='cmbRegion'></select><span id='spnErrorRegion' class='error'></span></div><div class='comuna'><span>Comuna</span><select id='cmbComuna'></select><span id='spnErrorComuna' class='error'></span></div><div class='telefono'><span>Teléfono</span><input type='text' id='txtTelefono' maxlength='11'><span id='spnErrorTelefono' class='error'></span></div><div class='nombreContacto'><span>Nombre Contacto</span><input type='text' id='txtNombreContacto' maxlength='50'><span id='spnErrorNombreContacto' class='error'></span></div><div class='emailContacto'><span>Email Contacto</span><input type='text' id='txtEmailContacto' maxlength='50'><span id='spnErrorEmailContacto' class='error'></span></div><div><button id='btnLimpiar'>Limpiar Formulario</button><button id='btnFacturaOrden'>Enviar Datos</button></div></form></div><br>");
var $htmlMessage = $('<span class="successFactura">Datos Factura Registrados Correctamente.</span>');

var regiones = [];
var comunas = [];
var emailContacto = '';

var entity = {
    id: "",
    PFAC_ORDERFORMID: "",
	PFAC_NROPEDIDO: "",
    PFAC_COMUNA: "",
    PFAC_DEPTO: "",
    PFAC_DIG: "",
    PFAC_DIRECCION: "",
    PFAC_GIRO: "",
    PFAC_EMAIL: "",
    PFAC_NUMERO: "",
    PFAC_RAZONSOCIAL: "",
    PFAC_REGION: "",
    PFAC_RUT: 0,
    PFAC_TELEFONO1: "",
    PFAC_TELEFONO2: "",
	PFAC_NOMBREFANTASIA: "",
	PFAC_NOMBRECONTACTO: ""
}
$(document).ready(function () {
	Factura();
});

function Factura()
{
	setTimeout(function(){
		if($(".span6.pull-right.payment-data").length > 0)
		{
			$checkBoxFactura2.insertBefore('.span6.pull-right.payment-data');
		}
		else{
			$checkBoxFactura2.insertBefore('.pull-right.payment-data.span12');
		}


		setCookie('esFactura', '', -1);
		setCookie('Id', '', -1);


		$("#chkCompraFactura").click(function(){
			var panel = $("#divChkCompraFactura").find('.accordion-factura-title');
			if($("#chkCompraFactura").is(':checked'))
			{
			  if(panel.length > 0)
			  {
				  $(panel[0]).addClass("accordion-toggle-active");
			  }
			  if($(".wrapFormFactura").length > 0)
			  {
				  $(".wrapFormFactura").show();
			  }
			  else
			  {
				  loadForm();
			  }
			  $("#payment-data-submit").hide();
			}
			else
			{
			  if(panel.length > 0)
			  {
				  $(panel[0]).removeClass("accordion-toggle-active");
			  }
			  $(".wrapFormFactura").hide();
			  $("#payment-data-submit").show();
			}
		});
	},2500);
}

function loadForm()
{
	//DESPLEGAR FORMULARIO
	$(".formFactura").before($htmlFormulario);
	loadRegionComuna();

	$("#txtRut").blur(function(){ isFormatValidRut(this); return false; });

	$("#btnFacturaOrden").click(function(){
		saveFacturaOrden();
		return false;
	});

	$("#btnLimpiar").click(function(){
		$("#divFormCompraFactura").find(':text, select').each(function(index){
			$(this).val('');
			$(this).attr("readonly", false);
		});

		return false;
	});

	vtexjs.checkout.getOrderForm().done(function(orderForm) {
		emailContacto = orderForm.clientProfileData.email;
		entity.PFAC_ORDERFORMID = orderForm.orderFormId;

		$.ajax({
			type: "GET",
			dataType: 'json',
			url: urlVtexMasterSearch.replace('{0}', emailContacto),
			success: function (data) {
				if(data.length > 0)
				{
					console.log('Data Recibo Vtex:');
					console.log(data);

					entity.PFAC_NROPEDIDO = 0;
					entity.PFAC_COMUNA = data[data.length - 1].PFAC_COMUNA;
					entity.PFAC_DEPTO = data[data.length - 1].PFAC_DEPTO;
					entity.PFAC_DIG = data[data.length - 1].PFAC_DIG;
					entity.PFAC_DIRECCION = data[data.length - 1].PFAC_DIRECCION;
					entity.PFAC_GIRO = data[data.length - 1].PFAC_GIRO;
					entity.PFAC_EMAIL = data[data.length - 1].PFAC_EMAIL;
					entity.PFAC_NUMERO = data[data.length - 1].PFAC_NUMERO;
					entity.PFAC_RAZONSOCIAL = data[data.length - 1].PFAC_RAZONSOCIAL;
					entity.PFAC_REGION = data[data.length - 1].PFAC_REGION;
					entity.PFAC_RUT = data[data.length - 1].PFAC_RUT;
					entity.PFAC_TELEFONO1 = data[data.length - 1].PFAC_TELEFONO1;
					entity.PFAC_TELEFONO2 = data[data.length - 1].PFAC_TELEFONO2;
					entity.PFAC_NOMBREFANTASIA = data[data.length - 1].PFAC_NOMBREFANTASIA;
					entity.PFAC_NOMBRECONTACTO = data[data.length - 1].PFAC_NOMBRECONTACTO;
					loadInfo();

					console.log('Data Entity:');
					console.log(entity);
				}
				else
				{
					entity.PFAC_EMAIL = emailContacto;
					$("#txtEmailContacto").val(emailContacto);
				}

				setMaxLenght();
			},
			error: function(error){
				console.log(error);
			}
		});
	});
}
function setMaxLenght()
{
	$("#txtRut").attr("maxlength", "10");
	$("#txtRazonSocial").attr("maxlength", "50");
	$("#txtGiro").attr("maxlength", "50");
	$("#txtNombreFantasia").attr("maxlength", "50");
	$("#txtDireccion").attr("maxlength", "80");
	$("#txtNumero").attr("maxlength", "20");
	$("#txtTelefono").attr("maxlength", "12");
	$("#txtEmailContacto").attr("maxlength", "50");
}

function saveFacturaOrden()
{
	if(isValidForm() && setInfo())
	{
		$.ajax({
		  accept: 'application/vnd.vtex.ds.v10+json',
		  contentType: 'application/json; charset=utf-8',
		  crossDomain: true,
		  type: "POST",
		  url: urlVtexMaster,
		  data: JSON.stringify(entity),
		  success: function(response)
		  {
			console.log(response);
			entity.id = response.Id;
			setCookie('Id', response.Id, 1);
			setCookie('esFactura', true, 1);

			$("#divChkCompraFactura").find(".button.golden").hide();
			$("#divFormCompraFactura").html($htmlMessage);


			$("#payment-data-submit").show();
		  }
		});
	}
}

function loadInfo()
{
	$("#txtRut").val(entity.PFAC_RUT + '-' + entity.PFAC_DIG);
	$("#txtRazonSocial").val(entity.PFAC_RAZONSOCIAL);
	$("#txtGiro").val(entity.PFAC_GIRO);
	$("#txtNombreFantasia").val(entity.PFAC_NOMBREFANTASIA);
	$("#txtDireccion").val(entity.PFAC_DIRECCION);
	$("#txtNumero").val(entity.PFAC_NUMERO);
	$("#cmbRegion").val(entity.PFAC_REGION);
	$("#txtTelefono").val(entity.PFAC_TELEFONO1);
	$("#txtNombreContacto").val(entity.PFAC_NOMBRECONTACTO);
	$("#txtEmailContacto").val(entity.PFAC_EMAIL);
	$("#txtNumeroDepto").val(entity.PFAC_DEPTO);

	$.each(comunas, function (index, value) {
		if (value.id_region == $("#cmbRegion").val())
			$("#cmbComuna").append(new Option(value.nombre, value.nombre));
	});
	$("#cmbComuna").val(entity.PFAC_COMUNA);

	$("#txtRut").attr('readonly', true);
	$("#txtEmailContacto").attr('readonly', true);
	$("#txtRazonSocial").attr('readonly', true);
}

function setInfo()
{
	entity.PFAC_REGION = $("#cmbRegion").val();
	entity.PFAC_COMUNA = $("#cmbComuna").val();
	entity.PFAC_RUT = $("#txtRut").val().split('-')[0];
	entity.PFAC_DIG = $("#txtRut").val().split('-')[1];
	entity.PFAC_DIRECCION = $("#txtDireccion").val();
	entity.PFAC_GIRO = $("#txtGiro").val();
	entity.PFAC_EMAIL = $("#txtEmailContacto").val();
	entity.PFAC_NUMERO = $("#txtNumero").val();
	entity.PFAC_RAZONSOCIAL = $("#txtRazonSocial").val();
	entity.PFAC_TELEFONO1 = $("#txtTelefono").val();
	entity.PFAC_NOMBREFANTASIA = $("#txtNombreFantasia").val();
	entity.PFAC_NOMBRECONTACTO = $("#txtNombreContacto").val();
	entity.PFAC_DEPTO = $("#txtNumeroDepto").val();

	return true;
}

function isValidForm()
{
	var isValid = true;
	$("#formCompraFactura").find(":text, select").each(function(index){
		var idError = $(this).attr('id').replace('txt', '#spnError').replace('cmb', '#spnError');


      	if($(idError).length != 0)
        {
          if($(this).val() == '' || $(this).val() == undefined)
          {
              $(idError).text('El campo es obligatorio.');
              isValid = false;
          }
          else
          {
              $(idError).text('');
          }
        }
	});


	var regExp_Mail = new RegExp(/^[a-z]+[a-z0-9\.\-\_]*@[a-z]+[a-z0-9\.]*\.[a-z]{2,3}$/i);
	var regExp_Telefono = new RegExp(/^[+0-9]{8,12}$/i);

	isFormatValidRut($("#txtRut"));

	if(!regExp_Mail.test($("#txtEmailContacto").val()))
	{
		isValid = false;
		$("#spnErrorEmail").text('El email no es valido.');
	}
	else
	{
		$("#spnErrorEmail").text('');
	}
	if(!regExp_Telefono.test($("#txtTelefono").val()))
	{
		isValid = false;
		$("#spnErrorTelefono").text('El teléfono no es valido.');
	}
	else
	{
		$("#spnErrorTelefono").text('');
	}

	return isValid;
}

function isFormatValidRut(control)
{
	$("#spnErrorRut").text('');

	var value = $(control).val().replace('-','').toLowerCase();
	if(value != '')
	{
		var rut = value.substring(0, value.length - 1);
		var dv = value.substring(value.length - 1, value.length);
		$(control).val(rut +'-'+dv);

		value = $(control).val();
		var regExp_Rut = new RegExp(/^[0-9]+-[0-9kK]{1}$/);
		if(!regExp_Rut.test(value))
		{
			$("#spnErrorRut").text('Rut con formato incorrecto');
		}

		var M=0,S=1;
		for(;rut;rut=Math.floor(rut/10))
		{
			S=(S+rut%10*(9-M++%6))%11;
		}

		var digV = S?S-1:'k';
		if(dv != digV)
		{
			$("#spnErrorRut").text('Rut incorrecto.');
		}
	}
	else
		$("#spnErrorRut").text('El campo es obligatorio.');
}

function loadRegionComuna()
{
	$.ajax({
		type: "GET",
		dataType: 'html',
		url: 'https://io.vtex.com.br/front.shipping-data/2.14.8/script/rule/CountryCHL.js',
		success: function (response) {
			var data = response.split("this.map=");
			var json = data[1].split("}}")[0];
			json = json + "}}";
			json = json.split('"').join('');
			json = json.split('{').join('{"');
			json = json.split(':').join('":');
			json = json.split(',').join(',"');

			data = $.parseJSON(json);
			console.log(data);

			for (region in data) {
				regiones.push({
					id: region,
					nombre: region
				});

				for (comuna in data[region]) {
					comunas.push({
						id: comuna,
						id_region: region,
						nombre: comuna,
						codigo: (data[region])[comuna]
					});
				}
			}

			$.each(regiones, function (index, value) {
				if (index == 0)
				{
					$("#cmbRegion").append(new Option("-- Seleccione una Región --", ""));
					$("#cmbComuna").append(new Option("-- Seleccione una Comuna --", ""));
				}
				$("#cmbRegion").append(new Option(value.nombre, value.id));
			});

			$("#cmbRegion").change(function () {
				var id = $(this).val();
				$('#cmbComuna').find('option').remove().end().append(new Option("-- Seleccione una Comuna --", ""));

				if (id != undefined && id != null && id != "") {
					$.each(comunas, function (index, value) {
						if (value.id_region == id)
							$("#cmbComuna").append(new Option(value.nombre, value.nombre));
					});
				}
			});
		}
	});
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}