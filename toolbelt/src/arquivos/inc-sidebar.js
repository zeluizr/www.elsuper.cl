// !function(a){function b(c,d){this.options=a.extend({},b.DEFAULTS,d),this.$sidebar=a(c),this.$sidebarInner=!1,this.$container=this.$sidebar.closest(this.options.containerSelector),this.affixedType="static",this._initialized=!1,this._breakpoint=!1,this._resizeListeners=[],this.dimensions={translateY:0,topSpacing:0,bottomSpacing:0,sidebarHeight:0,sidebarWidth:0,containerTop:0,containerHeight:0,viewportHeight:0,viewportTop:0,lastViewportTop:0},this.initialize()}function c(c){return this.each(function(){var d=a(this),e=a(this).data("stickySidebar");if(e||(e=new b(this,"object"==typeof c&&c),d.data("stickySidebar",e)),"string"==typeof c){if(void 0===e[c]&&-1===["destroy","updateSticky"].indexOf(c))throw new Error('No method named "'+c+'"');e[c]()}})}var d=a(window);b.VERSION="1.0.0",b.EVENT_KEY=".sticky",b.DEFAULTS={topSpacing:0,bottomSpacing:0,containerSelector:!1,innerWrapperSelector:".inner-wrapper-sticky",stickyClass:"is-affixed",resizeSensor:!0,minWidth:!1},b.isIE=function(){return Boolean(navigator.userAgent.match(/Trident/))},b.supportTransform=function(b){var c=!1,d=b?"perspective":"transform",e=d.charAt(0).toUpperCase()+d.slice(1),f="Webkit Moz O ms".split(" "),g=a("<support>").get(0).style;return a.each((d+" "+f.join(e+" ")+e).split(" "),function(a,b){if(void 0!==g[b])return c=b,!1}),c},b.prototype={initialize:function(){if(this.$sidebar.trigger("initialize"+b.EVENT_KEY),this.options.innerWrapperSelector&&(this.$sidebarInner=this.$sidebar.find(this.options.innerWrapperSelector),0===this.$sidebarInner.length&&(this.$sidebarInner=!1)),!this.$sidebarInner){var c=a('<div class="inner-wrapper-sticky" />');this.$sidebar.wrapInner(c),this.$sidebarInner=this.$sidebar.find(".inner-wrapper-sticky")}this.$container.length||(this.$container=this.$sidebar.parent()),"function"!=typeof this.options.topSpacing&&(this.options.topSpacing=parseInt(this.options.topSpacing)||0),"function"!=typeof this.options.bottomSpacing&&(this.options.bottomSpacing=parseInt(this.options.bottomSpacing)||0),this._widthBreakpoint(),this.calcDimensions(),this.stickyPosition(),this.bindEvents(),this._initialized=!0,this.$sidebar.trigger("initialized"+b.EVENT_KEY)},bindEvents:function(){this.options;d.on("resize"+b.EVENT_KEY,a.proxy(this._onResize,this)).on("scroll"+b.EVENT_KEY,a.proxy(this._onScroll,this)),this.$sidebar.on("update"+b.EVENT_KEY,a.proxy(this.updateSticky,this)),this.options.resizeSensor&&(this.addResizerListener(this.$sidebarInner,a.proxy(this.updateSticky,this)),this.addResizerListener(this.$container,a.proxy(this.updateSticky,this)))},_onScroll:function(a){this.$sidebar.is(":visible")&&this.stickyPosition()},_onResize:function(a){this._widthBreakpoint(),this.updateSticky()},calcDimensions:function(){if(!this._breakpoint){var a=this.dimensions;a.containerTop=this.$container.offset().top,a.containerHeight=this.$container.outerHeight(),a.containerBottom=a.containerTop+a.containerHeight,a.sidebarHeight=this.$sidebarInner.outerHeight(),a.sidebarWidth=this.$sidebar.outerWidth(),a.viewportHeight=d.prop("innerHeight"),this._calcDimensionsWithScroll()}},_calcDimensionsWithScroll:function(){var a=this.dimensions;a.sidebarLeft=this.$sidebar.offset().left,a.viewportTop=document.documentElement.scrollTop||document.body.scrollTop,a.viewportBottom=a.viewportTop+a.viewportHeight,a.viewportLeft=document.documentElement.scrollLeft||document.body.scrollLeft,a.topSpacing=this.options.topSpacing,a.bottomSpacing=this.options.bottomSpacing,"function"==typeof a.topSpacing&&(a.topSpacing=parseInt(a.topSpacing(this.$sidebar))||0),"function"==typeof a.bottomSpacing&&(a.bottomSpacing=parseInt(a.bottomSpacing(this.$sidebar))||0)},isSidebarFitsViewport:function(){return this.dimensions.sidebarHeight<this.dimensions.viewportHeight},isScrollingTop:function(){return this.dimensions.viewportTop<this.dimensions.lastViewportTop},getAffixType:function(){var a=this.dimensions,b=!1;this._calcDimensionsWithScroll();var c=a.sidebarHeight+a.containerTop,d=a.viewportTop+a.topSpacing,e=a.viewportBottom-a.bottomSpacing;return this.isScrollingTop()?d<=a.containerTop?(a.translateY=0,b="STATIC"):d<=a.translateY+a.containerTop?(a.translateY=d-a.containerTop,b="VIEWPORT-TOP"):!this.isSidebarFitsViewport()&&a.containerTop<=d&&(b="VIEWPORT-UNBOTTOM"):this.isSidebarFitsViewport()?a.sidebarHeight+d>=a.containerBottom?(a.translateY=a.containerBottom-c,b="CONTAINER-BOTTOM"):d>=a.containerTop&&(a.translateY=d-a.containerTop,b="VIEWPORT-TOP"):a.containerBottom<=e?(a.translateY=a.containerBottom-c,b="CONTAINER-BOTTOM"):c+a.translateY<=e?(a.translateY=e-c,b="VIEWPORT-BOTTOM"):a.containerTop+a.translateY<=d&&(b="VIEWPORT-UNBOTTOM"),a.lastViewportTop=a.viewportTop,b},_getStyle:function(c){if(void 0!==c){var d={inner:{},outer:{}},e=this.dimensions;switch(c){case"VIEWPORT-TOP":d.inner={position:"fixed",top:this.options.topSpacing,left:e.sidebarLeft-e.viewportLeft,width:e.sidebarWidth};break;case"VIEWPORT-BOTTOM":d.inner={position:"fixed",top:"auto",left:e.sidebarLeft,bottom:this.options.bottomSpacing,width:e.sidebarWidth};break;case"CONTAINER-BOTTOM":case"VIEWPORT-UNBOTTOM":d.inner={position:"absolute",top:e.containerTop+e.translateY},b.supportTransform(translate3d=!0)?d.inner={transform:"translate3d(0, "+e.translateY+"px, 0)"}:b.supportTransform()&&(d.inner={transform:"translate(0, "+e.translateY+"px)"})}switch(c){case"VIEWPORT-TOP":case"VIEWPORT-BOTTOM":case"VIEWPORT-UNBOTTOM":case"CONTAINER-BOTTOM":d.outer={height:e.sidebarHeight,position:"relative"}}return d.outer=a.extend({},{height:"",position:""},d.outer),d.inner=a.extend({},{position:"relative",top:"",left:"",bottom:"",width:"",transform:""},d.inner),d}},stickyPosition:function(c){if(this.$sidebar.is(":visible")&&!this._breakpoint){c=c||!1;var d=(this.options.topSpacing,this.options.bottomSpacing,this.getAffixType()),e=this._getStyle(d);if((this.affixedType!=d||c)&&d){var f=a.Event("affix."+d.replace("viewport-","")+b.EVENT_KEY);this.$sidebar.trigger(f),"static"===d?this.$sidebar.removeClass(this.options.stickyClass):this.$sidebar.addClass(this.options.stickyClass);var g=a.Event("affixed."+d.replace("viewport","")+b.EVENT_KEY);this.$sidebar.css(e.outer),this.$sidebarInner.css(e.inner),this.$sidebar.trigger(g)}else this._initialized&&this.$sidebarInner.css("left",e.inner.left);this.affixedType=d}},_widthBreakpoint:function(){d.innerWidth()<=this.options.minWidth?(this._breakpoint=!0,this.affixedType="static",this.$sidebar.removeAttr("style").removeClass(this.options.stickyClass),this.$sidebarInner.removeAttr("style")):this._breakpoint=!1},updateSticky:function(){this.calcDimensions(),this.stickyPosition(!0)},addResizerListener:function(b,c){var d=a(b);d.prop("resizeListeners")||(d.prop("resizeListeners",[]),this._appendResizeSensor(d)),d.prop("resizeListeners").push(c)},removeResizeListener:function(b,c){var d=a(b),e=d.prop("resizeListeners"),f=e.indexOf(c);if(this._resizeListeners.splice(f,1),d.prop("resizeListeners").length){var g=d.prop("resizeTrigger");a(g.contentDocument.defaultView).off("resize",this._resizeListener),g=d.find(g).remove()}},_appendResizeSensor:function(c){var d=a(c);"static"==d.css("position")&&d.css("position","relative");var e=a("<object>");e.attr("style","display: block; position: absolute; top: 0; left: 0; height: 100%; width: 100%;overflow: hidden; pointer-events: none; z-index: -1;"),e.prop("resizeElement",d[0]);var f=this;e.on("load",function(b){this.contentDocument.defaultView.resizeTrigger=this.resizeElement,a(this.contentDocument.defaultView).on("resize",f._resizeListener)}),e.prop("type","text/html"),b.isIE()&&e.prop(data,"about:blank"),d.prop("resizeTrigger",e.get(0)),d.append(e)},_resizeListener:function(a){var b=a.target||a.srcElement,c=b.resizeTrigger;c.resizeListeners.forEach(function(b){b.call(c,a)})},destroy:function(){d.off("resize"+b.EVENT_KEY).off("scroll"+b.EVENT_KEY),this.$sidebar.removeClass(this.options.stickyClass).css({minHeight:""}).off("update"+b.EVENT_KEY).removeData("stickySidebar"),this.$sidebarInner.css({position:"",top:"",left:"",bottom:"",width:"",transform:""}),this.options.resizeSensor&&(this.removeResizeListener(this.$sidebarInner,a.proxy(this.updateSticky,this)),this.removeResizeListener(this.$container,a.proxy(this.updateSticky,this)))}},a.fn.stickySidebar=c,a.fn.stickySidebar.Constructor=b;var e=a.fn.stickySidebar;a.fn.stickySidebar.noConflict=function(){return a.fn.stickySidebar=e,this},d.on("load",function(){a("[data-sticky-sidebar]").each(function(){var b=a(this),d=b.data()||{},e=b.closest("[data-sticky-sidebar-container]");e.length&&(d.containerSelector=e),c.call(b,d)})})}(jQuery);
// Sticky Plugin v1.0.4 for jQuery
// =============
// Author: Anthony Garand
// Improvements by German M. Bravo (Kronuz) and Ruud Kamphuis (ruudk)
// Improvements by Leonardo C. Daronco (daronco)
// Created: 02/14/2011
// Date: 07/20/2015
// Website: http://stickyjs.com/
// Description: Makes an element on the page stick on the screen as you scroll
//              It will only set the 'top' and 'position' of your element, you
//              might need to adjust the width in some cases.

(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else if (typeof module === 'object' && module.exports) {
        // Node/CommonJS
        module.exports = factory(require('jquery'));
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($) {
    var slice = Array.prototype.slice; // save ref to original slice()
    var splice = Array.prototype.splice; // save ref to original slice()

  var defaults = {
      topSpacing: 0,
      bottomSpacing: 0,
      className: 'is-sticky',
      wrapperClassName: 'sticky-wrapper',
      center: false,
      getWidthFrom: '',
      widthFromWrapper: true, // works only when .getWidthFrom is empty
      responsiveWidth: false,
      zIndex: 'inherit'
    },
    $window = $(window),
    $document = $(document),
    sticked = [],
    windowHeight = $window.height(),
    scroller = function() {
      var scrollTop = $window.scrollTop(),
        documentHeight = $document.height(),
        dwh = documentHeight - windowHeight,
        extra = (scrollTop > dwh) ? dwh - scrollTop : 0;

      for (var i = 0, l = sticked.length; i < l; i++) {
        var s = sticked[i],
          elementTop = s.stickyWrapper.offset().top,
          etse = elementTop - s.topSpacing - extra;

        //update height in case of dynamic content
        s.stickyWrapper.css('height', s.stickyElement.outerHeight());

        if (scrollTop <= etse) {
          if (s.currentTop !== null) {
            s.stickyElement
              .css({
                'width': '',
                'position': '',
                'top': '',
                'z-index': ''
              });
            s.stickyElement.parent().removeClass(s.className);
            s.stickyElement.trigger('sticky-end', [s]);
            s.currentTop = null;
          }
        }
        else {
          var newTop = documentHeight - s.stickyElement.outerHeight()
            - s.topSpacing - s.bottomSpacing - scrollTop - extra;
          if (newTop < 0) {
            newTop = newTop + s.topSpacing;
          } else {
            newTop = s.topSpacing;
          }
          if (s.currentTop !== newTop) {
            var newWidth;
            if (s.getWidthFrom) {
                padding =  s.stickyElement.innerWidth() - s.stickyElement.width();
                newWidth = $(s.getWidthFrom).width() - padding || null;
            } else if (s.widthFromWrapper) {
                newWidth = s.stickyWrapper.width();
            }
            if (newWidth == null) {
                newWidth = s.stickyElement.width();
            }
            s.stickyElement
              .css('width', newWidth)
              .css('position', 'fixed')
              .css('top', newTop)
              .css('z-index', s.zIndex);

            s.stickyElement.parent().addClass(s.className);

            if (s.currentTop === null) {
              s.stickyElement.trigger('sticky-start', [s]);
            } else {
              // sticky is started but it have to be repositioned
              s.stickyElement.trigger('sticky-update', [s]);
            }

            if (s.currentTop === s.topSpacing && s.currentTop > newTop || s.currentTop === null && newTop < s.topSpacing) {
              // just reached bottom || just started to stick but bottom is already reached
              s.stickyElement.trigger('sticky-bottom-reached', [s]);
            } else if(s.currentTop !== null && newTop === s.topSpacing && s.currentTop < newTop) {
              // sticky is started && sticked at topSpacing && overflowing from top just finished
              s.stickyElement.trigger('sticky-bottom-unreached', [s]);
            }

            s.currentTop = newTop;
          }

          // Check if sticky has reached end of container and stop sticking
          var stickyWrapperContainer = s.stickyWrapper.parent();
          var unstick = (s.stickyElement.offset().top + s.stickyElement.outerHeight() >= stickyWrapperContainer.offset().top + stickyWrapperContainer.outerHeight()) && (s.stickyElement.offset().top <= s.topSpacing);

          if( unstick ) {
            s.stickyElement
              .css('position', 'absolute')
              .css('top', '')
              .css('bottom', 0)
              .css('z-index', '');
          } else {
            s.stickyElement
              .css('position', 'fixed')
              .css('top', newTop)
              .css('bottom', '')
              .css('z-index', s.zIndex);
          }
        }
      }
    },
    resizer = function() {
      windowHeight = $window.height();

      for (var i = 0, l = sticked.length; i < l; i++) {
        var s = sticked[i];
        var newWidth = null;
        if (s.getWidthFrom) {
            if (s.responsiveWidth) {
                newWidth = $(s.getWidthFrom).width();
            }
        } else if(s.widthFromWrapper) {
            newWidth = s.stickyWrapper.width();
        }
        if (newWidth != null) {
            s.stickyElement.css('width', newWidth);
        }
      }
    },
    methods = {
      init: function(options) {
        return this.each(function() {
          var o = $.extend({}, defaults, options);
          var stickyElement = $(this);

          var stickyId = stickyElement.attr('id');
          var wrapperId = stickyId ? stickyId + '-' + defaults.wrapperClassName : defaults.wrapperClassName;
          var wrapper = $('<div></div>')
            .attr('id', wrapperId)
            .addClass(o.wrapperClassName);

          stickyElement.wrapAll(function() {
            if ($(this).parent("#" + wrapperId).length == 0) {
                    return wrapper;
            }
});

          var stickyWrapper = stickyElement.parent();

          if (o.center) {
            stickyWrapper.css({width:stickyElement.outerWidth(),marginLeft:"auto",marginRight:"auto"});
          }

          if (stickyElement.css("float") === "right") {
            stickyElement.css({"float":"none"}).parent().css({"float":"right"});
          }

          o.stickyElement = stickyElement;
          o.stickyWrapper = stickyWrapper;
          o.currentTop    = null;

          sticked.push(o);

          methods.setWrapperHeight(this);
          methods.setupChangeListeners(this);
        });
      },

      setWrapperHeight: function(stickyElement) {
        var element = $(stickyElement);
        var stickyWrapper = element.parent();
        if (stickyWrapper) {
          stickyWrapper.css('height', element.outerHeight());
        }
      },

      setupChangeListeners: function(stickyElement) {
        if (window.MutationObserver) {
          var mutationObserver = new window.MutationObserver(function(mutations) {
            if (mutations[0].addedNodes.length || mutations[0].removedNodes.length) {
              methods.setWrapperHeight(stickyElement);
            }
          });
          mutationObserver.observe(stickyElement, {subtree: true, childList: true});
        } else {
          if (window.addEventListener) {
            stickyElement.addEventListener('DOMNodeInserted', function() {
              methods.setWrapperHeight(stickyElement);
            }, false);
            stickyElement.addEventListener('DOMNodeRemoved', function() {
              methods.setWrapperHeight(stickyElement);
            }, false);
          } else if (window.attachEvent) {
            stickyElement.attachEvent('onDOMNodeInserted', function() {
              methods.setWrapperHeight(stickyElement);
            });
            stickyElement.attachEvent('onDOMNodeRemoved', function() {
              methods.setWrapperHeight(stickyElement);
            });
          }
        }
      },
      update: scroller,
      unstick: function(options) {
        return this.each(function() {
          var that = this;
          var unstickyElement = $(that);

          var removeIdx = -1;
          var i = sticked.length;
          while (i-- > 0) {
            if (sticked[i].stickyElement.get(0) === that) {
                splice.call(sticked,i,1);
                removeIdx = i;
            }
          }
          if(removeIdx !== -1) {
            unstickyElement.unwrap();
            unstickyElement
              .css({
                'width': '',
                'position': '',
                'top': '',
                'float': '',
                'z-index': ''
              })
            ;
          }
        });
      }
    };

  // should be more efficient than using $window.scroll(scroller) and $window.resize(resizer):
  if (window.addEventListener) {
    window.addEventListener('scroll', scroller, false);
    window.addEventListener('resize', resizer, false);
  } else if (window.attachEvent) {
    window.attachEvent('onscroll', scroller);
    window.attachEvent('onresize', resizer);
  }

  $.fn.sticky = function(method) {
    if (methods[method]) {
      return methods[method].apply(this, slice.call(arguments, 1));
    } else if (typeof method === 'object' || !method ) {
      return methods.init.apply( this, arguments );
    } else {
      $.error('Method ' + method + ' does not exist on jQuery.sticky');
    }
  };

  $.fn.unstick = function(method) {
    if (methods[method]) {
      return methods[method].apply(this, slice.call(arguments, 1));
    } else if (typeof method === 'object' || !method ) {
      return methods.unstick.apply( this, arguments );
    } else {
      $.error('Method ' + method + ' does not exist on jQuery.sticky');
    }
  };
  $(function() {
    setTimeout(scroller, 0);
  });
}));
